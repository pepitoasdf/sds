<?php

use Illuminate\Database\Seeder;
use App\Yearlevel;
class YearlevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
        	['level' => '1st' , 'description' => 'First Year'],
        	['level' => '2nd' , 'description' => 'Second Year'],
        	['level' => '3rd' , 'description' => 'Third Year'],
        	['level' => '4th' , 'description' => 'Fourth Year']
        ];

        foreach ($datas as $key => $data) {
        	Yearlevel::create(['shortname' => $data['level'], 'description' => $data['description']]);
        }
    }
}
