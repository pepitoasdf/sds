<?php

use Illuminate\Database\Seeder;
use App\Schoolyear;
use App\Course;
use App\SyCourse;
class SchoolyearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schoolyear::create(['shortname' => '2017-2018', 'description' => 'Schoolyear 2017-2018','status' => 'Applied', 'effective_date' => date('Y-m-d')]);
    }
}
