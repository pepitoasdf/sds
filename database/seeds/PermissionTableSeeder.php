<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\User;
use App\Role;
use App\TabMenu;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        		[
        			'name' => 'Dashboard',
        			'filter' => '_dashboard'
        		],
        		[
        			'name' => 'Student Directory',
        			'filter' => '_students'
        		],
        		[
        			'name' => 'Untrack Records',
        			'filter' => '_untrack'
        		],
        		[
        			'name' => 'Courses',
        			'filter' => '_courses'
        		],
        		[
        			'name' => 'Users',
        			'filter' => '_users'
        		],
                [
                    'name' => 'Logs',
                    'filter' => '_logs'
                ],
        		[
        			'name' => 'Roles',
        			'filter' => '_roles'
        		],
                [
                    'name' => 'School Year',
                    'filter' => '_sy'
                ]
        	];
        	TabMenu::truncate();
        	foreach ($data as $key => $value) {
        		TabMenu::create($value);
        	}
        $permissions = Permission::defaultPermissions();
        DB::table('permissions')->delete();
        DB::table('role_has_permissions')->delete();
        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }
        $role = Role::first();
        $role->syncPermissions(Permission::all());    	
    }
}
