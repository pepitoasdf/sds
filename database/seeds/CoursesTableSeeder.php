<?php

use Illuminate\Database\Seeder;
use App\Course;
class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $datas = [
            [
                'shortname' => 'BEEd',
                'description' => 'Bachelor of Science in Elementary Education'
            ],
            [
                'shortname' => 'BSEd',
                'description' => 'Bachelor of Science in Secondary Education'
            ],
            [
                'shortname' => 'BSHTE',
                'description' => 'Bachelor of Science in Home Technology Education'
            ],
            [
                'shortname' => 'BSIEd',
                'description' => 'Bachelor of Science in Industrial Education'
            ],
            [
                'shortname' => 'BSHRM',
                'description' => 'Bachelor of Science in Hotel Restaurant Management'
            ],
            [
                'shortname' => 'BSInfoTech',
                'description' => 'Bachelor of Science in Information Technology'
            ],
        	[
        		'shortname' => 'BSIT',
        		'description' => 'Bachelor of Science in Industrial Technology'
        	],
        	[
        		'shortname' => 'BSFi',
        		'description' => 'Bachelor of Science in Fisheries'
        	]
        ];

        foreach($datas as $data){
        	Course::create(['shortname' =>$data['shortname'], 'description' => $data['description']]);
        }
    }
}
