<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Permission;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);

        $ref = 0;
        // Ask for db migration refresh, default is no
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {
            // disable fk constrain check
            // \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            // Call the php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("Data cleared, starting from blank database.");

            // enable back fk constrain check
            // \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            $ref = 1;
        }

        $this->call(CoursesTableSeeder::class);
        $this->call(SchoolyearsTableSeeder::class);
        $this->call(YearlevelsTableSeeder::class);

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        $this->command->info('Default Permissions added.');

        // add roles
        foreach(['Admin','Stuff'] as $role) {
            $role = Role::firstOrCreate(['name' => trim($role)]);

            if( $role->name == 'Admin' ) {
                // assign all permissions
                $role->syncPermissions(Permission::all());
                $this->command->info('Admin granted all the permissions');
            } else {
                // for others by default only read access
                $role->syncPermissions(Permission::where('name', 'LIKE', 'view_%')->get());
            }

            // create one user for each role
            $this->createUser($role);
        }
        $this->command->info('Position Admin,Stuff added successfully');
        $this->command->warn('All done !');
    }
    private function createUser($role)
    {
        $user = User::firstOrCreate([
                'firstname' => $role->name,
                'lastname' => $role->name,
                'username' => strtolower($role->name),
                'password' => bcrypt('secret')
            ]);
        $user->assignRole($role->name);

        $this->command->info('Here is your '.strtolower($user->username).' details to login:');
        $this->command->warn('Username is : ' .$user->username);
        $this->command->warn('Password is : secret');
        $this->command->info('You can override your credentials in Admin page :)');
    }
}
