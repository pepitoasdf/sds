<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnStudentHeadersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('student_headers')){
            Schema::table('student_headers', function (Blueprint $table) {
               $table->string('altsig')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('student_headers', 'altsig')){
            Schema::table('student_headers', function (Blueprint $table) {
                $table->dropColumn('altsig');
            });
        }
    }
}
