<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSyIsOnStudentHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('student_headers')){
            Schema::table('student_headers', function (Blueprint $table) {
                $table->addColumn('integer','sy_id');
            });
            DB::table('schoolyears')->where('id',1)->update(['status' => 'Applied']);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('student_headers', 'sy_id')){
            Schema::table('student_headers', function (Blueprint $table) {
                $table->dropColumn('sy_id');
            });
        }
    }
}
