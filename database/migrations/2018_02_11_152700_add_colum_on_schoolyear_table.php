<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumOnSchoolyearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('schoolyears')){
            Schema::table('schoolyears', function (Blueprint $table) {
               $table->date('effective_date')->nullable();
            });
            DB::table('schoolyears')->update(['effective_date' => date('Y-m-d')]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('schoolyears', 'effective_date')){
            Schema::table('schoolyears', function (Blueprint $table) {
                $table->dropColumn('effective_date');
            });
        }
    }
}
