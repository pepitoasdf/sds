<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnguestpermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guest_permissions', function (Blueprint $table) {
               $table->string('app')->nullable();
        });

        Schema::table('users', function (Blueprint $table) {
               $table->string('token')->nullable();
        });
       // DB::table('guest_permissions')->update(['app' => strtotime('2018-02-16')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guest_permissions', function (Blueprint $table) {
            $table->dropColumn('app');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('token');
        });
    }
}
