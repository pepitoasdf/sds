<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStudentHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_number')->nullable();
            $table->integer('course_id')->nullable();
            $table->integer('yearlevel_id')->nullable();
            $table->string('section')->nullable();
            $table->string('student_firstname')->nullable();
            $table->string('student_lastname')->nullable();
            $table->string('student_middlename')->nullable();
            $table->enum('sex',array('Male','Female'))->default('Male');
            $table->enum('civil_status',array('Single','Married','Widow','Separated'))->default('Single');
            $table->date('bithdate')->nullable();

            $table->integer('bithplace_province_id')->nullable();
            $table->integer('bithplace_city_id')->nullable();
            $table->integer('bithplace_barangay_id')->nullable();
            $table->string('bithplace_street')->nullable();
            $table->string('bithplace_home')->nullable();
            $table->string('brother_sister_total')->nullable();

            $table->integer('home_address_province_id')->nullable();
            $table->integer('home_address_city_id')->nullable();
            $table->integer('home_address_barangay_id')->nullable();
            $table->string('home_address_street')->nullable();
            $table->string('home_address_home')->nullable();

            $table->integer('ta_province_id')->nullable();
            $table->integer('ta_city_id')->nullable();
            $table->integer('ta_barangay_id')->nullable();
            $table->string('ta_street')->nullable();
            $table->string('ta_home')->nullable();

            $table->string('father_firstname')->nullable();
            $table->string('father_lastname')->nullable();
            $table->string('father_middlename')->nullable();
            $table->string('father_occupation')->nullable();

            $table->string('mother_firstname')->nullable();
            $table->string('mother_lastname')->nullable();
            $table->string('mother_middlename')->nullable();
            $table->string('mother_occupation')->nullable();

            $table->string('highschool_graduate_school')->nullable();
            $table->integer('highschool_graduate_province_id')->nullable();
            $table->integer('highschool_graduate_city_id')->nullable();
            $table->integer('highschool_graduate_barangay_id')->nullable();
            $table->string('highschool_graduate_street')->nullable();
            $table->string('highschool_year_graduated')->nullable();

            $table->string('lastschool_attended_school')->nullable();
            $table->integer('lastschool_attended_province_id')->nullable();
            $table->integer('lastschool_attended_city_id')->nullable();
            $table->integer('lastschool_attended_barangay_id')->nullable();
            $table->string('lastschool_attended_street')->nullable();

            $table->string('nationality')->nullable();
            $table->string('religion')->nullable();
            $table->string('language_spoken')->nullable();

            $table->string('emergency_notify_name')->nullable();
            $table->string('emergency_notify_relation')->nullable();
            $table->integer('emergency_address_province_id')->nullable();
            $table->integer('emergency_address_city_id')->nullable();
            $table->integer('emergency_address_barangay_id')->nullable();
            $table->string('emergency_address_street')->nullable();
            $table->string('emergency_address_home')->nullable();
            $table->string('emergency_contact_number')->nullable();

            $table->string('course_major')->nullable();
            $table->text('user_photo')->nullable();
            $table->text('signature')->nullable();
            $table->string('contact_number')->nullable();

            $table->enum('status',array('Active','Inactive'))->default('Active');
            $table->enum('is_accepted',array('Yes','No'))->default('No');
            $table->enum('seen',array('Yes','No'))->default('No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_headers');
    }
}
