<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGuestPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('form',array('Yes','No'))->default('No');
            $table->enum('register',array('Yes','No'))->default('Yes');
            $table->timestamps();
        });
        DB::table('guest_permissions')->insert(['form' => 'No', 'register' => 'Yes']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_permissions');
    }
}
