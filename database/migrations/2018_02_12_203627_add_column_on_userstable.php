<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnUserstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {if (Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
               $table->string('photo')->nullable();
               $table->string('middlename')->nullable();
               $table->string('contact_number')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('photo');
            $table->dropColumn('middlename');
            $table->dropColumn('contact_number');
        });
    }
}
