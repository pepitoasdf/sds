# Laravel 5.4 based system for Student Directory System


## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__ (it has some seeded data for your testing)
- Run __php artisan serve__ and in URL type __localhost:8000__

## License

Basically, feel free to use and re-use any way you want.

---

