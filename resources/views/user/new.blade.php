@extends('layouts.master')
@section('title', ' - Users')
@section('content_header', 'Add User')
@section('content_header_link')
    <a class="breadcrumb-item" href="{{ route('users.index') }}">List</a>
    @can('add_users')
        <span class="breadcrumb-item active">Add User</span>
    @endcan
@endsection
@section('content')
<div class="card pd-20 pd-sm-40">
    <form class="form-horizontal" action="{{route('users.store')}}" method="POST">
        @include('user._form')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <label class="control-label text-right col-sm-4"></label>
                    <div class="col-md-8">
                        @can('add_users')
                            <button type="submit" name="update" value="update" class="btn btn-default btn-sm"> Save</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection