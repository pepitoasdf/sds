{{ csrf_field() }}
<div class="row">
    <div class="col-md-6">
        <div class="form-group row ">
            <label class="control-label text-right col-sm-4" for="firstname">First Name:</label>
            <div class="col-md-8">
                <input type="text" class="form-control form-control-sm {{ $errors->has('firstname') ? ' is-invalid' : '' }}" id="firstname" placeholder="First Name" required name="firstname" value="{{ isset($user) ? $user->firstname : old('firstname') }}">
                @if ($errors->has('firstname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('firstname') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row ">
            <label class="control-label text-right col-sm-4" for="lastname">Last Name:</label>
            <div class="col-md-8">
                <input type="text" class="form-control form-control-sm {{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname" placeholder="Last Name" required name="lastname" value="{{ isset($user) ? $user->lastname : old('lastname') }}">
                @if ($errors->has('lastname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row ">
            <label class="control-label text-right col-sm-4" for="username">Username:</label>
            <div class="col-md-8">
                <input type="text" class="form-control form-control-sm {{ $errors->has('username') ? ' is-invalid' : '' }}" id="username" name="username" placeholder="Username" required value="{{ isset($user) ? $user->username : old('username') }}">
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
@if(isset($user))
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row ">
                <label class="control-label text-right col-sm-4" for="oldpassword">Old Password:</label>
                <div class="col-md-8">
                    <input type="password" class="form-control form-control-sm  {{ $errors->has('oldpassword') ? ' is-invalid' : '' }}" id="oldpassword" placeholder="Current Password" name="oldpassword" required>
                    @if ($errors->has('oldpassword'))
                        <span class="help-block">
                            <strong>{{ $errors->first('oldpassword') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-md-6">
        <div class="form-group row ">
            <label class="control-label text-right col-sm-4" for="password">{{isset($user) ? 'New' : ''}} Password:</label>
            <div class="col-md-8">
                <input type="password" class="form-control form-control-sm {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="New Password" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row">
            <label class="control-label text-right col-sm-4" for="cpassword">Confirm Password:</label>
            <div class="col-md-8">
                <input type="password" class="form-control form-control-sm {{ $errors->has('password') ? ' is-invalid' : '' }}" id="cpassword" name="password_confirmation" placeholder="Confirm Password" required>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row">
            <label class="control-label text-right col-sm-4" for="roles">Roles:</label>
            <div class="col-md-8">
                <select class="form-control  {{ $errors->has('roles') ? ' is-invalid' : '' }} form-control-sm" id="roles" multiple="true" name="roles[]">
                    @if(isset($user))
                        @foreach($roles as $role)
                            <option value="{{$role->id}}" {{in_array($role->id,$user_role) ? 'selected' : ''}}>{{$role->name}}</option>
                        @endforeach
                    @else
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('roles'))
                    <span class="help-block">
                        <strong>{{ $errors->first('roles') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- Permissions -->
{{--

@if(isset($user))
    @include('shared._permissions', ['closed' => 'true', 'model' => $user ])
@endif

--}}
