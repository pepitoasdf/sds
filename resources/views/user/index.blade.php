@extends('layouts.master')
@section('title', ' - Users')
@section('content_header', 'Users')
@section('content_header_link')
    <span class="breadcrumb-item active">List</span>
    @can('add_users')
        <a class="breadcrumb-item" href="{{ route('users.create') }}">Add User</a>
    @endcan
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        @include('shared._alert')
        <h6 class="card-body-title">List</h6>
        <p class="mg-b-10 mg-sm-b-10">List of registered users.</p>
        <div class="card-title mg-b-10">
            @can('print_users')
                <a href="{{route('user-print')}}" target="_blank" class="btn btn-info btn-sm pull-left">Print</a>
            @endcan
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="data-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Created At</th>
                    @can('edit_users', 'delete_users')
                    <th class="text-center">Actions</th>
                    @endcan
                </tr>
                </thead>
                <tbody>
                @foreach($result as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->firstname }}  {{ $item->lastname }}</td>
                        <td>{{ $item->username }}</td>
                        <td>{{ $item->roles->implode('name', ', ') }}</td>
                        <td>{{ $item->created_at->toFormattedDateString() }}</td>

                        @can('edit_users')
                        <td class="text-center">
                            @include('shared._actions', [
                                'entity' => 'users',
                                'id' => $item->id
                            ])
                        </td>
                        @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $result->links() }}
    </div>
@endsection