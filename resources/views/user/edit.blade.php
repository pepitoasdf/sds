
@extends('layouts.master')
@section('title', ' - Users')
@section('content_header', 'Users')
@section('content_header_link')
    @can('view_users')
        <a class="breadcrumb-item" href="{{ route('users.index') }}">List</a>
    @endcan
    @can('add_users')
        <a class="breadcrumb-item" href="{{ route('users.create') }}">Add User</a>
    @endcan
@endsection

@section('content')
<div class="card pd-20 pd-sm-40">
    <h6 class="card-body-title">Edit</h6>
    <p class="mg-b-10 mg-sm-b-10">Edit User ID {{ str_pad($user->id,4,'0',STR_PAD_LEFT) }}</p>
    <form class="form-horizontal" action="{{route('users.update',$user->id)}}" method="POST">
        {{ method_field('PATCH') }}
        @include('user._form')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <label class="control-label text-right col-sm-4"></label>
                    <div class="col-md-8">
                        @can('edit_users')
                            <button type="submit" name="update" value="update" class="btn btn-default btn-sm"> Update</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

