@extends('layouts.master')
@section('title', ' - 403')
@section('content_header', 'Permission Denied')
@section('content_header_link')
  <span class="breadcrumb-item active">Permission Denied</span>
@endsection
@section('content')
<div class="card pd-20 pd-sm-40" style="min-height: 400px;">
      <div class="wd-lg-70p wd-xl-50p tx-center pd-x-40">
        <h1 class="tx-100 tx-xs-140 tx-normal tx-gray-800 mg-b-0">403!</h1>
        <h5 class="tx-xs-24 tx-normal tx-gray-600 mg-b-30 lh-5">HTTP 403 Permission Denied</h5>
        <p class="tx-16 mg-b-30">You do not have permission to view this page.</p>

        <div class="d-flex justify-content-center">
          <div class="d-flex wd-xs-300">
            <!-- <input type="text" class="form-control ht-40" placeholder="Search...">
            <button class="btn btn-default bd-0 mg-l-5 ht-40 pd-x-20">Search</button> -->
          </div>
        </div><!-- d-flex -->
      </div>
</div>
@endsection