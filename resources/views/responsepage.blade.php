
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="pepitoarielasdf">


    <title>SDS - PIT</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/pit-logo.png') }}">
    <!-- vendor css -->
    <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/select2/css/select2.min.css') }}" rel="stylesheet">

    <!-- magen-iot-admin CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/magen-iot-admin.css') }}">
    <!-- custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link href="{{ asset('assets/lib/spinner/spinner.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/font-awesome/css/animated.css') }}" rel="stylesheet">
  </head>

  <body>
        <div class="ht-100v d-flex align-items-center justify-content-center">
          <div class="wd-lg-70p wd-xl-50p tx-center pd-x-40">
            @include('shared._alert')
            <div class="d-flex justify-content-center">
              <div class="d-flex wd-xs-300">
                <a href="{{ route('guest_create') }}" class="btn btn-success btn-block"> New Directory Form</a>
              </div>
            </div><!-- d-flex -->
          </div>
        </div><!-- ht-100v -->
    
    <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/js/magen-iot-admin.js') }}"></script>
  </body>
</html>
