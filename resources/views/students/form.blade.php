@extends('layouts.form-master')
@section('content')
<div class="fix-width">
    <div class="row p-t-30">
        <div class="col-12">
			@include('students.form_include')
		</div>
    </div>
</div>
@endsection