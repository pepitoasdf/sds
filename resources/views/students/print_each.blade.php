<!DOCTYPE html>
<html>
<head>
	<title>{{$course->shortname}} - {{$student->student_firstname}}_{{$student->student_lastname}}</title>
	<style type="text/css">
		@page {
			margin: 8mm;
		}
		body{
			font-family: helvetica;
			font-size: 11px;
		}
		table {
			width: 100%;
		}
		.container{
			margin: 0px;
			padding: 0px;
		}
	</style>
</head>
<body>
	<div class="container">
		<table class="table" cellspacing="0" cellpadding="0">
			<tr>
				<td style="width: 20%;vertical-align: top;">
					<img src="assets/images/pit-logo.png" style="width: 90px;height: 90px;border: 1px solid #999;">
				</td>
				<td style="width: 60%;text-align: center;">
					<div>
						<div>Republic of the Philippines</div>
						<div><b>PALOMPON INSTITUTE OF TECHNOLOGY TABANGO CAMPUS</b></div>
						<div>Tabango, Leyte</div>
						<br/><br/>
						UNDERGRADUATE STUDENT'S DIRECTORY
					</div>
				</td>
				<td style="vertical-align:top;width: 20%;text-align: right">
					<img src="assets/images/profile/{{isset($student->user_photo) ? $student->user_photo : 'sq.png'}}" style="width: 96px;height: 96px;border: 1px solid #000;">
				</td>
			<tr>
			<tr>
				<td colspan="3" style="padding-top:10px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 15%;">1.) Student Number:</td>
							<td style="width: 20%; border-bottom: 1px solid #000;color:red">{{$student->id_number}}</td>
							<td style="width: 8%;">Course:</td>
							<td style="width: 27%; border-bottom: 1px solid #000;font-size: 9px;">{{$student->getcourse->description}}</td>
							<td style="width: 10%">Yr. & Sec.:</td>
							<td style="width: 20%; border-bottom: 1px solid #000">{{$student->yearlevel->shortname}} year {{$student->section ? ' & '.$student->section : ''}}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table"  cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 10%;">2.) Name:</td>
							<td style="width: 20%;text-align: center;border-bottom: 1px solid #000">{{$student->student_lastname}}</td>
							<td style="width: 20%;text-align: center;border-bottom: 1px solid #000">{{$student->student_firstname}}</td>
							<td style="width: 20%;text-align: center;border-bottom: 1px solid #000">{{$student->student_middlename}}</td>
							<td style="width: 15%;text-align: center;border-bottom: 1px solid #000">{{$student->sex}}</td>
							<td style="width: 15%;text-align: center;border-bottom: 1px solid #000">{{$student->civil_status}}</td>
						</tr>
						<tr style="font-size: 10px;">
							<td style="width: 10%;"></td>
							<td style="width: 20%;text-align: center">(Surname)</td>
							<td style="width: 20%;text-align: center">(First Name)</td>
							<td style="width: 20%;text-align: center">(Middle Name)</td>
							<td style="width: 15%;text-align: center">(Sex)</td>
							<td style="width: 15%;text-align: center">(Civil Status)</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 12%;">3.) Date of Birth:</td>
							<td style="width: 18%; border-bottom: 1px solid #000">{{date('M-d-Y',strtotime($student->birthdate))}}</td>
							<td style="width: 12%;">Place of Birth:</td>
							<td style="width: 28%; border-bottom: 1px solid #000;font-size: 9px;">{{$student->getbirth_place()}}</td>
							<td style="width: 20%">No. of Brothers & Sisters:</td>
							<td style="width: 10%; border-bottom: 1px solid #000;font-size: 9px;">{{$student->brother_sister_total}}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 14%;">4.) Home Address:</td>
							<td style="width: 36%; border-bottom: 1px solid #000;font-size: 11px;">{{$student->gethome_address()}}</td>
							<td style="width: 14%;">Tabango Address:</td>
							<td style="width: 36%; border-bottom: 1px solid #000;font-size: 11px;">{{$student->gettabango_address()}}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 14%;">5.) Father's Name:</td>
							<td style="width: 22%;text-align: center;border-bottom: 1px solid #000">{{$student->father_lastname}}</td>
							<td style="width: 22%;text-align: center;border-bottom: 1px solid #000">{{$student->father_firstname}}</td>
							<td style="width: 22%;text-align: center;border-bottom: 1px solid #000">{{$student->father_middlename}}</td>
							<td style="width: 20%;text-align: center;border-bottom: 1px solid #000">{{$student->father_occupation}}</td>
						</tr>
						<tr style="font-size: 10px;">
							<td style="width: 14%;"></td>
							<td style="width: 22%;text-align: center">(Surname)</td>
							<td style="width: 22%;text-align: center">(First Name)</td>
							<td style="width: 22%;text-align: center">(Middle Name)</td>
							<td style="width: 20%;text-align: center">(Occupation)</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 14%;">6.) Mother's Name:</td>
							<td style="width: 22%;text-align: center;border-bottom: 1px solid #000">{{$student->mother_lastname}}</td>
							<td style="width: 22%;text-align: center;border-bottom: 1px solid #000">{{$student->mother_firstname}}</td>
							<td style="width: 22%;text-align: center;border-bottom: 1px solid #000">{{$student->mother_middlename}}</td>
							<td style="width: 20%;text-align: center;border-bottom: 1px solid #000">{{$student->mother_occupation}}</td>
						</tr>
						<tr style="font-size: 10px;">
							<td style="width: 14%;"></td>
							<td style="width: 22%;text-align: center">(Surname)</td>
							<td style="width: 22%;text-align: center">(First Name)</td>
							<td style="width: 22%;text-align: center">(Middle Name)</td>
							<td style="width: 20%;text-align: center">(Occupation)</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 20%;">7.) High School Graduated:</td>
							<td style="width: 30%;text-align: center;border-bottom: 1px solid #000">{{$student->highschool_graduate_school}}</td>
							<td style="width: 35%;text-align: center;border-bottom: 1px solid #000">{{$student->gethighschool_address()}}</td>
							<td style="width: 15%;text-align: center;border-bottom: 1px solid #000">{{$student->highschool_year_graduated}}</td>
						</tr>
						<tr style="font-size: 10px;">
							<td style="width: 20%;"></td>
							<td style="width: 30%;text-align: center">Name of School</td>
							<td style="width: 35%;text-align: center">Address</td>
							<td style="width: 15%;text-align: center">Year Graduated</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="2">8.) Last School Attended(If student taken College Courses)</td>
						</tr>
						<tr>
							<td style="width: 30%;text-align: center;border-bottom: 1px solid #000">{{$student->lastschool_attended_school}}&nbsp;</td>
							<td style="width: 70%;text-align: center;border-bottom: 1px solid #000">{{$student->get_lastschool_address()}}&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 30%;text-align: center">Name of School</td>
							<td style="width: 70%;text-align: center">Address</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 12%;">9.) Nationality:</td>
							<td style="width: 15%; border-bottom: 1px solid #000">{{$student->nationality}}</td>
							<td style="width: 9%;">Religion:</td>
							<td style="width: 20%; border-bottom: 1px solid #000">{{$student->religion}}</td>
							<td style="width: 20%">Language Spoken at Home:</td>
							<td style="width: 24%; border-bottom: 1px solid #000;font-size: 10px;">{{$student->language_spoken}}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 27%;">10.) In case of emergency, please notify:</td>
							<td style="width: 36%; border-bottom: 1px solid #000">{{$student->emergency_notify_name}}</td>
							<td style="width: 10%;">Relation:</td>
							<td style="width: 27%; border-bottom: 1px solid #000">{{$student->emergency_notify_relation}}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:5px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 17%;">11.) Emergency Address:</td>
							<td style="width: 45%; border-bottom: 1px solid #000">{{$student->get_emergency_address()}}</td>
							<td style="width: 8%;">Contact No.:</td>
							<td style="width: 28%; border-bottom: 1px solid #000">{{$student->emergency_contact_number}}</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:10px;padding-bottom:10px;">
					<br/><br/>
					<hr />
					<br/>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="padding-top:20px;">
					<table class="table" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 70%;vertical-align: top">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 20%;vertical-align: top">
											<img src="assets/images/pit-logo.png" style="width: 90px;height: 90px;border: 1px solid #999;">
										</td>
										<td style="width: 80%;text-align: center;">
											<div>Republic of the Philippines</div>
											<div><b>PALOMPON INSTITUTE OF TECHNOLOGY TABANGO CAMPUS</b></div>
											<div><i>OFFICE OF STUDENT AFFAIRS</i></div>
											<br/>
											<div>Tabango, Leyte</div>
											<br/>
											<br/>
											<div>STUDENT ID FORM</div>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-top: 35px;">
											<table class="table" cellspacing="0" cellpadding="0">
												<tr>
													<td style="width: 26%; padding-top:4px;padding-bottom: 4px;">Date:</td>
													<td style="width: 44%; border-bottom: 1px solid #000;padding-top:4px;padding-bottom: 4px;">{{date('M-Y-d',strtotime($student->id_number))}}</td>
													<td>
												</tr>
												<tr>
													<td style="width: 26%; padding-top:4px;padding-bottom: 4px;">Student Id Number:</td>
													<td style="width: 44%; border-bottom: 1px solid #000;padding-top:4px;padding-bottom: 4px;color:red">{{$student->id_number}}</td>
													<td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
							<td style="width: 30%;vertical-align: top;text-align: right">
								<img src="assets/images/profile/{{isset($student->user_photo) ? $student->user_photo : 'sq.png'}}" style="width: 192px;height: 192px;border: 1px solid #000;">
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom: 2px;">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 5%;padding-top:2px;">Name:</td>
										<td style="width: 95%; border-bottom: 1px solid #000;padding-top:2px;">{{$student->student_firstname}} {{$student->student_middlename}} {{$student->student_lastname}}</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom: 2px;">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 11%;padding-top:5px;">Course & Year:</td>
										<td style="width: 89%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->getcourse->description}} {{isset($student->yearlevel) ? ', ' . $student->yearlevel->shortname : ''}}</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom: 2px;">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 5%;padding-top:5px;">Major:</td>
										<td style="width: 45%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->course_major}}</td>
										<td style="width: 13%;padding-top:5px;">Official Receipt #:</td>
										<td style="width: 37%; border-bottom: 1px solid #000;padding-top:5px;">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom: 2px;">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 11%;padding-top:5px;">Date of Birth:</td>
										<td style="width: 39%; border-bottom: 1px solid #000;padding-top:5px;">{{date('M-d-Y',strtotime($student->bithdate))}}</td>
										<td style="width: 12%;padding-top:5px;">Contact Number:</td>
										<td style="width: 32%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->contact_number}}</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom: 2px;">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 10%;padding-top:5px;">Home Address:</td>
										<td style="width: 90%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->gethome_address()}}</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom: 2px;">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 26%;padding-top:5px;">In Case of Emergency Please Notify:</td>
										<td style="width: 35%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->emergency_notify_name}}</td>
										<td style="width: 9%;padding-top:5px;">Relation:</td>
										<td style="width: 30%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->emergency_notify_relation}}</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom: 2px;">
								<table class="table" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width: 12%;padding-top:5px;">Contact number:</td>
										<td style="width: 38%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->emergency_contact_number}}</td>
										<td style="width: 7%;padding-top:4px;padding-bottom: 4px;">Address:</td>
										<td style="width: 43%; border-bottom: 1px solid #000;padding-top:5px;">{{$student->get_emergency_address()}}</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br/>
		<div style="margin-top:20px;height: 100px;border: 1px solid #000;text-align: center;">
			@if(isset($student->signature))
				<img src="{{$student->signature}}" alt="No signature" style="height: 100px; width: 400px;">
			@elseif(isset($student->altsig))
				<img src="/assets/images/altsig/{{$student->altsig}}" alt="No signature" style="height: 100px; width: 400px;">
			@endif
		</div>
	</div>
</body>
</html>