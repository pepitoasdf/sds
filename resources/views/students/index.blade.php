@extends('layouts.master')
@section('title', ' - Student Directory')
@section('content_header', 'Student Directory')
@section('content_header_link')
    <span class="breadcrumb-item active">List</span>
    @can('add_students')
        <a class="breadcrumb-item" href="{{ route('directory-create',$id) }}">Add Student</a>
    @endcan
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        @include('shared._alert')
        <h6 class="card-body-title">{{$course->shortname}}</h6>
        <p class="mg-b-10 mg-sm-b-10">{{ ucfirst(strtolower($course->description)) }}</p>
        <div class="card-title mg-b-10">
            @can('print_students')
                <a href="{{ route('directory-printall',$course->id.'?filter='.$fid.'&filter2='.$fid2) }}" target="_blank" class="btn btn-info btn-sm pull-left">Print All</a>

                <a style="margin-left: 10px;" href="{{ route('directory-print_id',$course->id.'?filter='.$fid.'&filter2='.$fid2) }}" target="_blank" class="btn btn-info btn-sm pull-left">Print IDs</a>
            @endcan
            <span class="pull-right">
                <select class="form-control form-control-sm sy_filter" style="width: 100px;">
                    @foreach(App\Schoolyear::all() as $sy)
                        @if($fid)
                            <option value="{{$sy->id}}" {{$fid == $sy->id ? 'selected' : ''}}>{{$sy->shortname}}</option>
                        @else
                            <option value="{{$sy->id}}">{{$sy->shortname}}</option>
                        @endif
                    @endforeach
                </select>
            </span>
            <span class="pull-right" style="padding-right: 10px;padding-left:10px;font-weight: normal;">
               <label style="line-height: 25px;">S.Y. :</label> 
            </span>
            <span class="pull-right">
                <select class="form-control form-control-sm yl_filter" style="width: 100px;">
                    @foreach(App\Yearlevel::all() as $yl)
                        @if($fid2)
                            <option value="{{$yl->id}}" {{$fid2 == $yl->id ? 'selected' : ''}}>{{$yl->shortname}}</option>
                        @else
                            <option value="{{$yl->id}}">{{$yl->shortname}}</option>
                        @endif
                    @endforeach
                </select>
            </span>
            <span class="pull-right" style="padding-right: 10px;font-weight: normal;">
               <label style="line-height: 25px;">Year Level :</label> 
            </span>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th width="15%">ID Number</th>
                        <th colspan="3" width="30%">Student Name</th>
                        <th width="8%">Gender</th>
                        <th width="25%">Home Address</th>
                        <th width="12%">Status</th>
                        @can('edit_students','print_students')
                            <th width="10%">
                                Action
                            </th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach($student as $s)
                        <tr>
                            <td>{{$s->id_number}}</td>
                            <td width="13%">{{isset($s->student_lastname) ? $s->student_lastname : ''}}</td>
                            <td width="13%">{{isset($s->student_firstname) ? $s->student_firstname : ''}}</td>
                            <td width="4%">
                                @if(isset($s->student_middlename))
                                    {{ (strlen($s->student_middlename) > 0) ? $s->student_middlename[0].'.' : ''}}
                                @endif
                            </td>
                            <td>{{$s->sex}}</td>
                            <td>{{$s->gethome_address()}}</td>
                            <td>{{$s->status}}</td>
                             @can('edit_students','print_students')
                                <td>
                                    @can('edit_students')
                                        <a href="{{ route('directory-edit',['id' => $course->id, 'ref_id' => $s->id]) }}" class="btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                    @endcan &nbsp;
                                    @can('print_students')
                                        <a href="{{ route('directory-print_each',['id' => $course->id, 'ref_id' => $s->id]) }}" target="_blank" class="btn-info btn-sm" title="Print"><i class="fa fa-print"></i></a>
                                    @endcan
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $student->links() }}
    </div>
    @push('append_js')
        <script type="text/javascript">
            $('.sy_filter').change(function(e){
                e.preventDefault();
                var id = $(this).val();
                var id2 = $('.yl_filter').val();
                window.location.href = "?filter="+id+"&filter2="+id2;
            });
            $('.yl_filter').change(function(e){
                e.preventDefault();
                var id2 = $(this).val();
                var id = $('.sy_filter').val();
               window.location.href = "?filter="+id+"&filter2="+id2;
            });
        </script>
    @endpush
@endsection
