@extends('layouts.master')
@section('title', ' - Student Directory')
@section('content_header', 'Student Directory Form')
@section('content_header_link')
	@can('view_students')
    	<a class="breadcrumb-item" href="{{ route('directory-index',$id) }}">List</a>
    @endcan
    <span class="breadcrumb-item active">Add Student</span>
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">Student Information</h6>
        <p class="mg-b-10 mg-sm-b-10">All fields which are marked with asterisk(<span class="tx-danger">*</span>) are mandatory</p>
        @include('students.form_include')
    </div>
@endsection
