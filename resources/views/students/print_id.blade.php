<!DOCTYPE html>
<html>
<head>
	<title>{{$course->shortname}}</title>
	<style type="text/css">
		@page {
			margin: 10mm;
		}
		body{
			font-family: helvetica;
			font-size: 11px;
		}
		table {
			width: 100%;
		}
		.container{
			margin: 0px;
			padding: 0px;
		}

	</style>
</head>
<body>
	<table class="table" cellspacing="0" cellpadding="0">
		<tr>
			<td style="width: 20%;vertical-align: top;">
				&nbsp;
			</td>
			<td style="width: 60%;text-align: center;">
				<div>
					<div>Republic of the Philippines</div>
					<div><b>PALOMPON INSTITUTE OF TECHNOLOGY TABANGO CAMPUS</b></div>
					<div>Tabango, Leyte</div>
					<br/><br/>
					<b>LIST OF STUDENTS ID</b>
				</div>
			</td>
			<td style="vertical-align:top;width: 20%;text-align: right">
				<img src="assets/images/pit-logo.png" style="width: 90px;height: 90px;">
			</td>
		<tr>
	</table>
	<br/>
	<br/>
	<div>
		@if($sy)
			<span><b>School Year :</b> {{ ($sy) ? $sy->shortname : '' }}</span> <br/>
		@endif
		<span><b>Course      : </b> {{$course->description}} {{($yl) ? ' / '.$yl->shortname : ''}}</span>
	</div>
	<br/>
	<table class="table" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th colspan="3" width="53%" style="text-align: left">Names</th>
				<th style="text-align: left  "> Student IDs</th>
			</tr>
		</thead>
		<tbody>
			@foreach($students as $s)
				<tr>
					<td>{{isset($s->student_lastname) ? $s->student_lastname : ''}}</td>
                    <td>{{isset($s->student_firstname) ? $s->student_firstname : ''}}</td>
                    <td>
                        @if(isset($s->student_middlename))
                            {{ (strlen($s->student_middlename) > 0) ? $s->student_middlename[0].'.' : ''}}
                        @endif
                    </td>
                    <td>{{$s->id_number}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>