@extends('layouts.master')
@section('title', ' - Dashboard')
@section('content_header', 'Dashboard')
@section('content_header_link')
    <span class="breadcrumb-item active">Home</span>
@endsection
@section('content')
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="row row-sm">
      <div class="col-lg-6" style="display: none">
        <div class="card">
          <div class="card-body pd-b-0">
            <!-- <img src="../img/senc.svg" width="60" class="card-icon"/> -->
            <span class="card-icon icon ion-ios-people-outline" style="font-size: 60px;"></span>
            <h6 class="card-body-title tx-12 tx-spacing-2 mg-b-20">Untrack Students</h6>
            <h2 class="tx-roboto tx-inverse">{{number_format($untrackTotal)}} <small>Untrack</small></h2>
            @if($untrackTotal > 0)
              <p class="tx-12"><span class="tx-success">{{ number_format((($untrackYesterday / $untrackTotal) * 100),2) }}%</span> sent from yesterday</p>
            @else
              <p class="tx-12"><span class="tx-success">0%</span> sent from yesterday</p>
            @endif
          </div><!-- card-body -->
          <div id="rs1" class="ht-50 ht-sm-70 mg-r--1"></div>
        </div><!-- card -->
      </div><!-- col-6 -->
      <div class="col-lg-6">
        <div class="card">
          <div class="card-body pd-b-0">
           <!--  <img src="../img/energy.svg" width="60" class="card-icon"/> -->
            <span class="card-icon icon ion-ios-people-outline" style="font-size: 60px;"></span>
            <h6 class="card-body-title tx-12 tx-spacing-2 mg-b-20">Accepted Students</h6>
            <h2 class="tx-roboto tx-inverse">{{number_format($acceptedTotal)}} <small>Accepted</small></h2>
            @if($acceptedTotal > 0)
              <p class="tx-12"><span class="tx-success">{{ number_format((($acceptedYesterday / $acceptedTotal) * 100),2) }}%</span> accepted from yesterday</p>
            @else
              <p class="tx-12"><span class="tx-success">0%</span> accepted from yesterday</p>
            @endif
          </div><!-- card-body -->
          <div id="rs2" class="ht-50 ht-sm-70 mg-r--1"></div>
        </div><!-- card -->
      </div><!-- col-6 -->
    </div><!-- row -->
    <div class="card pd-20 pd-sm-40 mg-t-20">
      <h6 class="card-body-title">Monthly Chart</h6>
      <p class="mg-b-20 mg-sm-b-30">Number Of Students Registered Every Month</p>
      <canvas id="chartBar3" height="380"></canvas>
    </div><!-- card -->
    <div class="card pd-20 pd-sm-40 mg-t-20">
      <h6 class="card-body-title">School Year Chart</h6>
      <p class="mg-b-20 mg-sm-b-30">Number Of Students Registered Every School Year</p>
      <canvas id="chartBar4" height="380"></canvas>
    </div><!-- card -->
  </div><!-- col-12 -->
</div><!-- row -->
@push('append_css')
  <link href="{{ asset('assets/lib/rickshaw/rickshaw.css') }}" rel="stylesheet">
@endpush
@push('append_js')
<!--   <script src="{{ asset('assets/lib/rickshaw/d3.js') }}"></script> -->
  <script src="{{ asset('assets/lib/rickshaw/rickshaw.min.js') }}"></script>
  <script src="{{ asset('assets/lib/rickshaw/Chart.js') }}"></script>
  <script src="{{ asset('assets/lib/rickshaw/ResizeSensor.js') }}"></script>
  <script type="text/javascript">
    var ctb4 = document.getElementById('chartBar4').getContext('2d');
    new Chart(ctb4, {
      type: 'horizontalBar',
      data: {
        labels: {!! json_encode($sylist) !!},
        datasets: [{
          label: '# of Students',
          data: {!! json_encode($sytotal) !!},
          backgroundColor: [
            '#f1116a',
            '#017afd',
            '#0063d3',
            '#f3a344',
            '#95c75d',
            '#17A2B8',
            'red',
            '#6F42C1'
          ]
        }]
      },
      options: {
        legend: {
          display: false,
            labels: {
              display: false
            }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 10
            }
          }],
          xAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 11,
              max: 50000
            }
          }]
        }
      }
    });

    var ctb3 = document.getElementById('chartBar3').getContext('2d');
    new Chart(ctb3, {
      type: 'horizontalBar',
      data: {
        labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        datasets: [{
          label: '# of Students',
          data: {!! json_encode($m) !!},
          backgroundColor: [
            '#f1116a',
            '#017afd',
            '#0063d3',
            '#f3a344',
            '#95c75d',
            '#17A2B8',
            'red',
            '#6F42C1',
            'orange',
            'cyan',
            'blue',
            'green'
          ]
        }]
      },
      options: {
        legend: {
          display: false,
            labels: {
              display: false
            }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 10
            }
          }],
          xAxes: [{
            ticks: {
              beginAtZero:true,
              fontSize: 11,
              max: 1000
            }
          }]
        }
      }
    });
  </script>
@endpush
@endsection
