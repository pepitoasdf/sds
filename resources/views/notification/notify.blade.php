@foreach($student as $name)
    <?php
      $date = \Carbon\Carbon::parse($name->created_at);
      $now = \Carbon\Carbon::now();

      $diffSec = $date->diffInSeconds($now);

      $diffMin = $date->diffInMinutes($now);

      $diffHrs = $date->diffInHours($now);

      $diffDays = $date->diffInDays($now);

      $diffMon = $date->diffInMonths($now);

      $diffYrs = $date->diffInYears($now);


      if($diffSec > 0 && $diffSec <= 59){
        $diff = $now->subSeconds($diffSec)->diffForHumans(); 
      }
      elseif($diffMin > 0 && $diffMin <= 59){
        $diff = $now->subMinutes($diffMin)->diffForHumans(); 
      }
      elseif($diffHrs > 0 && $diffHrs <= 23){
        $diff = $now->subHours($diffHrs)->diffForHumans(); 
      }
      elseif($diffDays > 0 && $diffDays <= 28){
        $diff = $now->subDays($diffDays)->diffForHumans(); 
      }
      elseif($diffMon > 0 && $diffMon <= 11){
        $diff = $now->subMonths($diffMon)->diffForHumans(); 
      }
      elseif($diffYrs > 0){
        $diff = $now->subYears($diffYrs)->diffForHumans(); 
      }
      else{
        $diff = 'Just now';
      }

    ?>
    <a href="{{ route('recent-view',$name->id) }}" class="media-list-link read">
      <div class="media pd-x-20 pd-y-15">
        @if(isset($name->user_photo))
            <img src="{{asset('/assets/images/profile/'.$name->user_photo)}}" class="wd-40 rounded-circle" alt="{{$name->student_firstname[0]}}">
        @else
            <img src="{{ asset('assets/images/default-profile.png') }}" class="wd-40 rounded-circle" alt="">
        @endif
        <div class="media-body">
          <p class="tx-13 mg-b-0"><strong class="tx-medium">{{$name->student_firstname}} {{$name->student_lastname}}</strong> {{$name->getcourse->description}}</p>
          <span class="tx-12">{{ $diff }}</span>
        </div>
      </div><!-- media -->
    </a>
@endforeach