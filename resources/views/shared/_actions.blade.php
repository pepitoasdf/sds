@can('edit_'.$entity)
    <a title="Edit" href="{{ route($entity.'.edit', [str_singular($entity) => $id])  }}" class="btn btn-sm btn-primary">
        <i class="fa fa-pencil"></i></a>
@endcan

@can('delete_'.$entity)
    <form action="{{route($entity.'.destroy', [str_singular($entity) => $id])}}" style="display: inline" onsubmit="return confirm('Do you really want to delete it?')" method="POST">
    	{{csrf_field()}}
    	{{ method_field('DELETE') }}
        <button title="Delete" type="submit" class="btn-delete btn btn-sm btn-danger">
            <i class="fa fa-trash-o"></i>
        </button>
    </form>
@endcan