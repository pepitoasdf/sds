@if(Session::has('alert-success'))
	<div class="alert alert-success" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="d-flex align-items-center justify-content-start">
			<i class="icon ion-ios-checkmark alert-icon tx-24 mg-t-5 mg-xs-t-0"></i>
			<span><strong>Well done!</strong> {{ Session::get('alert-success') }}</span>
		</div><!-- d-flex -->
	</div><!-- alert -->
@elseif(Session::has('alert-info'))
	<div class="alert alert-info" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="d-flex align-items-center justify-content-start">
			<i class="icon ion-ios-information alert-icon tx-24 mg-t-5 mg-xs-t-0"></i>
			<span><strong>Heads up!</strong> {{ Session::get('alert-info') }}</span>
		</div><!-- d-flex -->
	</div><!-- alert -->
@elseif(Session::has('alert-warning'))
	<div class="alert alert-warning" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="d-flex align-items-center justify-content-start">
			<i class="icon ion-alert-circled alert-icon tx-24 mg-t-5 mg-xs-t-0"></i>
			<span><strong>Warning!</strong>  {{ Session::get('alert-warning') }}</span>
		</div><!-- d-flex -->
	</div><!-- alert -->
@elseif(Session::has('alert-danger'))
	<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="d-flex align-items-center justify-content-start">
			<i class="icon ion-ios-close alert-icon tx-24"></i>
			<span><strong>Oh snap!</strong>  {{ Session::get('alert-danger') }}</span>
		</div><!-- d-flex -->
	</div><!-- alert -->
@endif
