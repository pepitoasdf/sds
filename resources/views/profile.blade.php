@extends('layouts.master')
@section('title', ' - Profile')
@section('content_header', 'User Profile')
@section('content_header_link')
    <li class="breadcrumb-item active">Profile</li>
@endsection
@section('content')
    <form action="" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('shared._alert')
    <div class="row">
          <div class="col-md-4 col-lg-3">
            <label class="content-left-label">Your Profile Photo</label>
            <figure class="edit-profile-photo">
              <input  name="photo" data-default-file="{{$user->photo ? '/assets/images/users/'.$user->photo : '/assets/images/default-profile.png'}}" accept="image/*" type="file" id="input-file-now-custom-2" class="dropify " data-height="230"/>
              <!-- <figcaption style="z-index: 2000">
                <a href="" class="btn btn-dark">Update Photo</a>
              </figcaption> -->
              <button class="btn btn-default btn-sm update_photo" type="submit" name="update_photo" value="update_photo">Update Photo</button>
            </figure>
            <label class="content-left-label mg-t-30">Your Role(s)</label>
            <ul class="edit-profile-tag-list">
                @foreach(auth()->user()->roles as $role)
                  <li>
                    <span>{{$role->name}}</span>
                    <a href="{{ route('roles.index') }}"><i class="icon ion-ios-locked tx-20"></i></a>
                  </li>
                @endforeach
            </ul>

          </div><!-- col-3 -->
          <div class="col-md-8 col-lg-9 mg-t-30 mg-md-t-0">
            <label class="content-left-label">Login Information</label>
            <div class="card bg-gray-200 bd-0">
              <div class="edit-profile-form">
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Username: <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" required name="username" placeholder="Enter username" type="text" value="{{$user->username}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Old Password: <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" required="" name="oldpassword" placeholder="Enter old password" type="password" value="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">New Password: <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" required="" name="newpassword" placeholder="Enter new password" type="password" value="">
                  </div>
                </div>
                <div class="form-group row mg-b-0">
                  <label class="col-sm-3 form-control-label">Confirm New Password: <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" required="" name="conpassword" placeholder="Confirm new password" type="password" value="">
                  </div>
                </div>
              </div><!-- wd-60p -->
            </div><!-- card -->

            <hr class="invisible">

            <label class="content-left-label">Personal Information</label>
            <div class="card bg-gray-200 bd-0">
              <div class="edit-profile-form">
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Firstname: <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" required="" name="firstname" placeholder="Enter firstname" type="text" value="{{$user->firstname}}">
                  </div>
                </div><!-- form-group -->
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Middle Name: </label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" name="middlename" placeholder="Enter Middle name" type="text" value="{{$user->middlename}}">
                  </div>
                </div><!-- form-group -->
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Lastname: <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" required="" name="lastname" placeholder="Enter lastname" type="text" value="{{$user->lastname}}">
                  </div>
                </div><!-- form-group -->
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Contact Number: <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                    <input class="form-control form-control-sm" required="" name="contact_number" placeholder="Enter contact number" type="text" value="{{$user->contact_number}}">
                  </div>
                </div><!-- form-group -->
              </div><!-- wd-60p -->
            </div><!-- card -->
            <hr class="invisible">
            <button class="btn btn-default btn-sm">Update</button>
          </div><!-- col-9 -->
    </div><!-- row -->
    </form>
    @push('append_js')
        <script src="{{ asset('assets/lib/signature-pad/js/app.js') }}"></script>
        <script src="{{ asset('assets/lib/dropify/dropify.min.js') }}"></script>
        <script type="text/javascript">
            $('.dropify').dropify();

            $('.update_photo').click(function(e){
              $('input[type=text]').removeAttr('required');
              $('input[type=password]').removeAttr('required');
              $('select').removeAttr('required');
            });
        </script>
    @endpush

    @push('append_css')
        <link href="{{ asset('assets/lib/dropify/dropify.min.css') }}" rel="stylesheet">
    @endpush
@endsection