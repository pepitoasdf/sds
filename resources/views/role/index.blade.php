@extends('layouts.master')
@section('title', ' - Roles')
@section('content_header', 'Roles')
@section('content_header_link')
    <li class="breadcrumb-item active">System Roles</li>
    @can('add_roles')
        <li class="breadcrumb-item "><a href="{{ route('roles.create') }}">Create Roles</a></li>
    @endcan
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        @include('shared._alert')
        <h6 class="card-body-title">List</h6>
        <p class="mg-b-10 mg-sm-b-10">List of user's role.</p>
        {{-- @include('role.guest') --}}
        @forelse ($roles as $k => $role)
            <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <div class="card-header" role="tab" id="heading{{$role->id}}">
                        <h6 class="mg-b-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$role->id}}" aria-expanded="true" aria-controls="collapse{{$role->id}}" class="tx-gray-800 transition">
                                {{$role->name}}
                            </a>
                        </h6>
                    </div>
                    <div id="collapse{{$role->id}}" class="collapse show" role="tabpanel" aria-labelledby="heading{{$role->id}}">
                        <div class="card-body">
                            <div class="row">
                                @foreach($menus as $menu)
                                    @if($menu->filter != "_untrack")
                                    <div class="col-sm-3">
                                        <fieldset style="min-height: 200px">
                                            <legend>
                                                {{ucfirst($menu->name)}}
                                            </legend>
                                            <div class="form-layout">
                                                <div class="row mg-b-5">
                                                    @foreach($menu->crop() as $k => $perm)
                                                        <?php
                                                            $per_found = null;

                                                            if( isset($role) ) {
                                                                $per_found = $role->hasPermissionTo($perm->name);
                                                            }

                                                            if( isset($user)) {
                                                                $per_found = $user->hasDirectPermission($perm->name);
                                                            }
                                                            $permname = explode('_', $perm->name)
                                                        ?>
                                                        <div class="col-sm-12">
                                                            <label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }}">
                                                                <input type="checkbox" class="js-switch" data-color="#009efb" data-size="small" name="permissions[]" disabled="" {{($per_found) ? 'checked' : ''}} value="{{$perm->name}}"/>
                                                                {{ ucfirst($permname[0]) }} {{-- ucfirst($permname[1]) --}}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr/>
                                    @if($role->name != "Admin")
                                        @can('edit_roles')
                                            <a href="{{ route('roles.edit',$role->id) }}" class="pull-right btn btn-info btn-sm">
                                                Edit
                                            </a>
                                        @endcan
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
        
        @endforelse
    </div>
    @push('append_css')
        <link rel="stylesheet" href="{{ asset('assets/lib/switchery/dist/switchery.min.css') }}">
    @endpush

    @push('append_js')
        <script src="{{ asset('assets/lib/switchery/dist/switchery.min.js') }}"></script>
        <script type="text/javascript">
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            elems.forEach(function(html) {
              var switchery = new Switchery(html,{ size: 'small' });
            });
        </script>
    @endpush
@endsection