@extends('layouts.master')
@section('title', ' - Roles')
@section('content_header', $role->name . ' Role')
@section('content_header_link')
    @can('view_roles')
        <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">System Roles</a></li>
    @endcan
    @can('add_roles')
        <li class="breadcrumb-item "><a href="{{ route('roles.create') }}">Create Roles</a></li>
    @endcan
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        @if ($role->name != 'Guest')
            <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="heading{{$role->id}}">
                    <h6 class="mg-b-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$role->id}}" aria-expanded="true" aria-controls="collapse{{$role->id}}" class="tx-gray-800 transition">
                            {{$role->name}}
                        </a>
                    </h6>
                </div>
                <div id="collapse{{$role->id}}" class="collapse show" role="tabpanel" aria-labelledby="heading{{$role->id}}">
                    <div class="card-body">
                        <form action="{{ route('roles.update',$role->id) }}" method="POST" name="edit-role" id="edit-role">
                        {{ method_field('PUT') }}
                        {{csrf_field()}}
                        <div class="row">
                             @foreach($menus as $menu)
                                <div class="col-sm-3">
                                    <fieldset style="min-height: 200px">
                                        <legend>
                                            {{ucfirst($menu->name)}}
                                        </legend>
                                        <div class="form-layout">
                                            <div class="row mg-b-5">
                                                @foreach($menu->crop() as $k => $perm)
                                                    <?php
                                                        $per_found = null;

                                                        if( isset($role) ) {
                                                            $per_found = $role->hasPermissionTo($perm->name);
                                                        }

                                                        if( isset($user)) {
                                                            $per_found = $user->hasDirectPermission($perm->name);
                                                        }
                                                        $permname = explode('_', $perm->name)
                                                    ?>
                                                    <div class="col-sm-12">
                                                        <label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }}">
                                                            <input type="checkbox" class="js-switch" data-color="#009efb" data-size="small" name="permissions[]" {{($per_found) ? 'checked' : ''}} value="{{$perm->name}}"/>
                                                            {{ ucfirst($permname[0]) }} {{-- ucfirst($permname[1]) --}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr/>
                                @can('edit_roles')
                                    <button type="submit" class="pull-right btn btn-default btn-sm" form="edit-role">
                                        Update
                                    </button>
                                @endcan
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        @else
            <form action="{{ route('roles.update',0) }}" method="POST" name="edit-role" id="edit-role">
                {{ method_field('PUT') }}
                        {{csrf_field()}}
            <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <div class="card-header" role="tab" id="heading0">
                        <h6 class="mg-b-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0" class="tx-gray-800 transition">
                                Guest
                            </a>
                        </h6>
                    </div>
                    <div id="collapse0" class="collapse show" role="tabpanel" aria-labelledby="heading0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <fieldset>
                                        <legend>
                                            Guest Permission
                                        </legend>
                                        <div class="form-layout">
                                            <div class="row mg-b-5">
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="checkbox" class="js-switch" data-color="#009efb" data-size="small" name="guestform" {{(App\GuestPermission::get_guest()->form == 'Yes') ? 'checked' : ''}} value="{{App\GuestPermission::get_guest()->form}}"/>
                                                        Allow Form
                                                    </label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="checkbox" class="js-switch" data-color="#009efb" data-size="small" name="guestregister" {{(App\GuestPermission::get_guest()->register == 'Yes') ? 'checked' : ''}} value="{{App\GuestPermission::get_guest()->register}}"/>
                                                        Allow Register
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr/>
                                    @can('edit_roles')
                                        <button type="submit" class="pull-right btn btn-default btn-sm" form="edit-role">
                                            Update
                                        </button>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        @endif
    </div>
    @push('append_css')
        <link rel="stylesheet" href="{{ asset('assets/lib/switchery/dist/switchery.min.css') }}">
    @endpush

    @push('append_js')
        <script src="{{ asset('assets/lib/switchery/dist/switchery.min.js') }}"></script>
        <script type="text/javascript">
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            elems.forEach(function(html) {
              var switchery = new Switchery(html,{ size: 'small' });
            });
        </script>
    @endpush
@endsection