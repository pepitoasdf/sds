@extends('layouts.master')
@section('title', ' - Roles')
@section('content_header', 'Create Roles')
@section('content_header_link')
    @can('view_roles')
        <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">System Roles</a></li>
    @endcan
    <li class="breadcrumb-item active">Create Roles</li>
@endsection

@section('content')
<div class="card pd-20 pd-sm-40" style="min-height: 450px;">
    <form class="form-horizontal" action="{{route('roles.store')}}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label text-right col-sm-4" for="name">Role Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control form-control-sm" id="name" placeholder="Name" required name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row ">
                    <label class="control-label text-right col-sm-4"></label>
                    <div class="col-md-8">
                        @can('add_roles')
                            <button type="submit" class="btn btn-sm btn-default btn-sm">
                                Save
                            </button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection