            <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <div class="card-header" role="tab" id="heading0">
                        <h6 class="mg-b-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0" class="tx-gray-800 transition">
                                Guest
                            </a>
                        </h6>
                    </div>
                    <div id="collapse0" class="collapse show" role="tabpanel" aria-labelledby="heading0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <fieldset>
                                        <legend>
                                            Guest Permission
                                        </legend>
                                        <div class="form-layout">
                                            <div class="row mg-b-5">
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="checkbox" class="js-switch" data-color="#009efb" data-size="small" name="guest[]" disabled="" {{(App\GuestPermission::get_guest()->form == 'Yes') ? 'checked' : ''}} value="{{App\GuestPermission::get_guest()->form}}"/>
                                                        Allow Form
                                                    </label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="checkbox" class="js-switch" data-color="#009efb" data-size="small" name="guest[]" disabled="" {{(App\GuestPermission::get_guest()->register == 'Yes') ? 'checked' : ''}} value="{{App\GuestPermission::get_guest()->register}}"/>
                                                        Allow Register
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr/>
                                        @can('edit_roles')
                                            <a href="{{ route('roles.edit', 0) }}" class="pull-right btn btn-info btn-sm">
                                                Edit
                                            </a>
                                        @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>