
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="pepitoarielasdf">


    <title>SDS - PIT</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/pit-logo.png') }}">
    <!-- vendor css -->
    <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/select2/css/select2.min.css') }}" rel="stylesheet">

    <!-- magen-iot-admin CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/magen-iot-admin.css') }}">
    <!-- custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link href="{{ asset('assets/lib/spinner/spinner.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/font-awesome/css/animated.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/dropify/dropify.min.css') }}" rel="stylesheet">
  </head>

  <body>
        @include('shared._alert')
        <div class="card pd-20 pd-sm-40" style="padding-left: 150px; padding-right: 150px">
            @if(App\GuestPermission::get_guest()->form == 'Yes')
                <h6 class="card-body-title">Student Information</h6>
                <p class="mg-b-10 mg-sm-b-10">All fields which are marked with asterisk(<span class="tx-danger">*</span>) are mandatory</p>
                @include('form')
            @else
                <div class="alert alert-default" role="alert">
                    <div class="d-flex align-items-center justify-content-start">
                        <i class="icon ion-ios-information alert-icon tx-24 mg-t-5 mg-xs-t-0"></i>
                        <span><strong>Heads up!</strong> This Page was currently not available. </span>
                    </div><!-- d-flex -->
                </div><!-- alert -->
            @endif
        </div>
    
    <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/lib/parsleyjs/parsley.js') }}"></script>
    <script src="{{ asset('assets/js/magen-iot-admin.js') }}"></script>
    <script src="{{ asset('assets/lib/signature-pad/js/signature_pad.js') }}"></script>
    <script src="{{ asset('assets/lib/signature-pad/js/app.js') }}"></script>
    <script src="{{ asset('assets/lib/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $('.dropify').dropify();

        $(".province").select2({
            placeholder: 'Provinces',
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "/places/provinces",
                dataType : 'json',
                data : function (params) {
                    return {
                        name: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });

        $('.city_mun').select2({
            placeholder: 'City/Municipality',
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "/places/citymun",
                dataType : 'json',
                data : function (params) {
                    var prov = $(this).closest('div.row').find('.province').val();
                    return {
                        name: params.term,
                        ref_id: prov
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });

        $('.barangay').select2({
            placeholder: 'Barangay',
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "/places/barangays",
                dataType : 'json',
                data : function (params) {
                    var prov = $(this).closest('div.row').find('.province').val();
                    var cm = $(this).closest('div.row').find('.city_mun').val();
                    return {
                        name: params.term,
                        ref_id1: prov,
                        ref_id2: cm
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });
        $('form#sdsform').parsley().on('form:error', function (formInstance) {
           $('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#dc3545'});
        });

        $('.province').on('select2:select', function (evt) {
            $(this).closest('div').find('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#28a745'});
            $(this).closest('div.row').find('.city_mun').val(null).trigger('change.select2');
            $(this).closest('div.row').find('.barangay').val(null).trigger('change.select2');
        });
        $('.city_mun').on('select2:select', function (evt) {
            $(this).closest('div').find('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#28a745'});
            $(this).closest('div.row').find('.barangay').select2("val", null);
        });
        $('.barangay').on('select2:select', function (evt) {
            $(this).closest('div').find('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#28a745'});
        });
    </script>
  </body>
</html>
