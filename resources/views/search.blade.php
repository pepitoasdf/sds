@extends('layouts.master')
@section('title', ' - Search')
@section('content_header', 'Search')
@section('content_header_link')
        <li class="breadcrumb-item active">List</li>
@endsection
@section('content')
<div class="card pd-20 pd-sm-40" style="min-height: 400px;">
    <h6 class="card-body-title">Search Keyword ({{ $keyword }})</h6>
    <p class="mg-b-10 mg-sm-b-10">About {{number_format(count($data))}} {{count($data) > 1 ? 'results' : 'result'}} found in Database</p>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th width="15%">ID No.</th>
                    <th colspan="3" width="30%">Student Name</th>
                    <th colspan="3" width="55%">Course</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $s)
                    <tr>
                        <td><a href="{{ route('directory-edit',['id' => $s->course_id, 'ref_id' => $s->id]) }}">{{$s->id_number}}</a></td>
                        <td width="13%">{{isset($s->student_firstname) ? $s->student_firstname : ''}}</td>
                        <td width="4%">{{isset($s->student_middlename) ? $s->student_middlename[0] : ''}}.</td>
                        <td width="13%">{{isset($s->student_lastname) ? $s->student_lastname : ''}}</td>
                        <td>{{$s->description}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $data->links() }}
</div>
@endsection