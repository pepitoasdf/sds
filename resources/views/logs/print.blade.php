<<!DOCTYPE html>
<html>
<head>
	<title>Log's List</title>
</head>
<style type="text/css">
	table{
		width: 100%;
	}
	th,td{
		padding-left: 7px;
		padding-right: 7px;
	}
	h2{
		text-align: center;
	}
</style>
<body>
	<div>
		<h2>List Of Logs</h2>
	</div>
	<br/>
	<table class="table table-striped table-hover" id="data-table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Tabs</th>
                    <th>Action</th>
                    <th>User</th>
                    <th>Date/Time</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $log)
                <tr>
                    <td style="text-align: left;">{{str_pad($log->id,4,'0',STR_PAD_LEFT)}}</td>
                    <td>{{ $log->main_tab }} / {{ $log->sub_tab }}</td>
                    <td>{{ $log->action }}</td>
                    <td>{{ $log->getusers->username }}</td>
                    <td>{{ date('Y-m-d h:i:s A',strtotime($log->created_at)) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
</body>
</html>