@extends('layouts.master')
@section('title', ' - Logs')
@section('content_header', 'System Logs')
@section('content_header_link')
    <span class="breadcrumb-item active">List</span>
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        <h6 class="card-body-title">User's Logs</h6>
        <p class="mg-b-10 mg-sm-b-10">List of User's Logs</p>
        <div class="card-title mg-b-10">
            @can('print_logs')
                <a href="{{ route('logs.print') }}" target="_blank" class="btn btn-info btn-sm pull-left">Print</a>
            @endcan
            <span class="pull-right">
                <select class="form-control form-control-sm filter" style="width: 100px;">
                    @foreach(App\User::all() as $user)
                        @if($fid)
                            <option value="{{$user->id}}" {{$fid == $user->id ? 'selected' : ''}}>{{$user->username}}</option>
                        @else
                            <option value="{{$user->id}}">{{$user->username}}</option>
                        @endif
                    @endforeach
                </select>
            </span>
            <span class="pull-right" style="padding-right: 10px;font-weight: normal;">
               <label style="line-height: 25px;">Filter :</label> 
            </span>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td width="10%" style="text-align: right;">No.</td>
                        <th width="30%">Tabs</th>
                        <th width="20%">Action</th>
                        <th width="20%">User</th>
                        <th width="20%">Date/Time</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td style="text-align: right;">{{str_pad($log->id,4,'0',STR_PAD_LEFT)}}</td>
                            <td>{{ $log->main_tab }} / {{ $log->sub_tab }}</td>
                            <td>{{ $log->action }}</td>
                            <td>{{ $log->getusers->username }}</td>
                            <td>{{ date('Y-m-d h:i:s A',strtotime($log->created_at)) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $logs->links() }}
    </div>
    @push('append_js')
        <script type="text/javascript">
            $('.filter').change(function(e){
                e.preventDefault();
                var id = $(this).val();
                window.location.href = "?filter="+id;
            });
        </script>
    @endpush
@endsection
