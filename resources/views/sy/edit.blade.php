@extends('layouts.master')
@section('title', ' - School Year')
@section('content_header', 'Edit School Year')
@section('content_header_link')
        @can('view_sy')
            <a class="breadcrumb-item" href="{{ route('sy.index') }}">List</a>
        @endcan
        <span class="breadcrumb-item active">Add School Year</span>
@endsection
@section('content')
<div class="card pd-20 pd-sm-40" style="min-height: 400px;">
    <h6 class="card-body-title">Edit</h6>
    <p class="mg-b-10 mg-sm-b-10">Edit School Year ID {{ str_pad($data->id,4,'0',STR_PAD_LEFT) }}</p>
    <form class="form-horizontal" action="{{route('sy.update',$data->id)}}" method="POST">
        {{ method_field('PATCH') }}
        @include('sy._form')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <label class="control-label text-right col-sm-4"></label>
                    <div class="col-md-8">
                        @can('edit_sy')
                            <button type="submit" class="btn btn-default btn-sm"> Update</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection