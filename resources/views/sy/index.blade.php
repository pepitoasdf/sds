@extends('layouts.master')
@section('title', ' - School Year')
@section('content_header', 'School Year')
@section('content_header_link')
    <li class="breadcrumb-item active">List</li>
    @can('add_sy')
        <li class="breadcrumb-item "><a href="{{ route('sy.create') }}">Add School Year</a></li>
    @endcan
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        @include('shared._alert')
        <h6 class="card-body-title">List</h6>
        <p class="mg-b-10 mg-sm-b-10">List of School Year</p>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td width="10%" >ID</td>
                        <th width="18%">Shortname</th>
                        <th width="45%">Description</th>
                        <th width="20%" >Effective Date</th>
                        <th width="7%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sy as $s)
                        <tr>
                            <td style="">{{str_pad($s->id,4,'0',STR_PAD_LEFT)}}</td>
                            <td>{{$s->shortname}}</td>
                            <td width="13%">{{$s->description}}</td>
                            @if($s->effective_date)
                                <td>{{date('F d, Y',strtotime($s->effective_date))}}</td>
                            @else
                                <td></td>
                            @endif
                            <td>
                                @can('edit_sy')
                                <a href="{{ route('sy.edit',$s->id) }}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $sy->links() }}
    </div>
@endsection