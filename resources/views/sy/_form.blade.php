{{ csrf_field() }}
<div class="row">
    <div class="col-md-6">
        <div class="form-group row ">
            <label class="control-label text-right col-sm-4" for="shortname">Short name:</label>
            <div class="col-md-8">
                <input type="text" class="form-control form-control-sm {{ $errors->has('shortname') ? ' is-invalid' : '' }}" id="shortname" placeholder="Abbreviation" required name="shortname" value="{{ isset($data) ? $data->shortname : old('shortname') }}">
                @if ($errors->has('shortname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('shortname') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row ">
            <label class="control-label text-right col-sm-4" for="description">Description:</label>
            <div class="col-md-8">
                <textarea class="form-control form-control-sm {{ $errors->has('description') ? ' is-invalid' : '' }}" placeholder="Description" required="" name="description">{{ isset($data) ? $data->description : old('description') }}</textarea>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row ">
            <label class="control-label text-right col-sm-4" for="effective_date">Effective Date:</label>
            <div class="col-md-8">
                <input type="date" required="" @if(isset($data)) disabled="" @endif name="effective_date" class="effective_date form-control form-control-sm {{ $errors->has('effective_date') ? ' is-invalid' : '' }}" value="{{isset($data->effective_date) ? $data->effective_date : ''}}">
                @if ($errors->has('effective_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('effective_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>

