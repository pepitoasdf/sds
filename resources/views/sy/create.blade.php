@extends('layouts.master')
@section('title', ' - School Year')
@section('content_header', 'Add School Year')
@section('content_header_link')
    @can('view_sy')
        <a class="breadcrumb-item" href="{{ route('sy.index') }}">List</a>
    @endcan
    @can('add_sy')
        <span class="breadcrumb-item active">Add School Year</span>
    @endcan
@endsection
@section('content')
<div class="card pd-20 pd-sm-40" style="min-height: 400px;">
    <form class="form-horizontal" action="{{route('sy.store')}}" method="POST">
        @include('sy._form')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <label class="control-label text-right col-sm-4"></label>
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-default btn-sm"> Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@push('append_js')
    <script type="text/javascript">
        $('.effective_date').on('change',function(){
            var refdate = "{{isset($effective->effective_date) ? $effective->effective_date : ''}}";
            var getdate = $(this).val();
            var refdate2 = new Date(refdate);
            var getdate2 = new Date(getdate);
            if(refdate){
                if(getdate2 <= refdate2){
                    alert('Effective Date must greater than last effective date.');
                    $(this).val('');
                    $(this).focus();
                    return false;
                }
            }
        });
    </script>
@endpush
@endsection