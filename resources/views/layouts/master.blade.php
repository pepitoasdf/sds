
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="pepitoarielasdf">


    <title>SDS @yield('title')</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/pit-logo.png') }}">
    <!-- vendor css -->
    <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/select2/css/select2.min.css') }}" rel="stylesheet">

    <!-- magen-iot-admin CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/magen-iot-admin.css') }}">
    <!-- custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link href="{{ asset('assets/lib/spinner/spinner.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/font-awesome/css/animated.css') }}" rel="stylesheet">
    @stack('append_css')
  </head>

  <body>

    <!-- ##### SIDEBAR LOGO ##### -->
    <div class="kt-sideleft-header">
      <div class="kt-logo">
        <a href="">SDS - PIT
          <small>
              @hasanyrole(App\Role::all())
                {{ Auth::user()->roles[0]->name == "Admin" ? 'Administrator' : Auth::user()->roles[0]->name }}
              @endhasanyrole
          </small>
        </a>
      </div>
      <div id="ktDate" class="kt-date-today"></div>
      <div class="input-group kt-input-search">
        <input type="text" class="form-control g-search" placeholder="Search...">
        <span class="input-group-btn mg-0">
          <button class="btn btnsearch"><i class="fa fa-search"></i></button>
        </span>
      </div><!-- input-group -->
    </div><!-- kt-sideleft-header -->

    <!-- ##### SIDEBAR MENU ##### -->
    <div class="kt-sideleft">
      <label class="kt-sidebar-label">Navigation</label>
      <ul class="nav kt-sideleft-menu">
        @can('view_dashboard')
          <li class="nav-item ">
            <a href="{{ route('dashboard-index') }}" class="nav-link {{ Request::is('home*') ? 'active' : '' }}">
              <i class="icon ion-ios-home-outline"></i>
              <span>Dashboard</span>
            </a>
          </li><!-- nav-item -->
        @endcan
        @can('view_students')
          <li class="nav-item">
            <a href="" class="nav-link with-sub {{ Request::is('directory*') ? 'active' : '' }}">
              <i class="icon ion-ios-folder-outline"></i>
              <span>Student Directory</span>
            </a>
            <ul class="nav-sub">
              @foreach(App\Course::all() as $courses)
                <li class="nav-item"><a href="/directory/{{$courses->id}}" class="nav-link {{ Request::is('directory/'.$courses->id.'*') ? 'active' : '' }}"><i class="icon ion-ios-circle-outline"></i> {{$courses->shortname}}</a></li>
              @endforeach
            </ul>
          </li><!-- nav-item -->
        @endcan
        {{-- @can('view_untrack') --}}
          <!-- <li class="nav-item">
            <a href="{{ route('recent-list') }}" class="nav-link {{ Request::is('untrack*') ? 'active' : '' }}">
              <i class="icon ion-clipboard"></i>
              <span>Untrack Records</span>
            </a>
          </li> --><!-- nav-item -->
        {{-- @endcan --}}
        @can('view_users','view_roles','view_logs')
          <li class="nav-item">
            <a href="" class="nav-link with-sub {{ Request::is('systems*') ? 'active' : '' }}">
              <i class="icon ion-ios-people-outline"></i>
              <span>User Management</span>
            </a>
            <ul class="nav-sub">
                @can('view_users')
                  <li class="nav-item">
                    <a href="{{ route('users.index') }}" class="nav-link {{ Request::is('systems/users*') ? 'active' : '' }}"><i class="icon ion-ios-person-outline"></i> Users
                    </a>
                  </li>
                @endcan
                @can('view_logs')
                  <li class="nav-item">
                    <a href="{{ route('logs.index') }}" class="nav-link {{ Request::is('systems/logs*') ? 'active' : '' }}"><i class="icon ion-ios-person-outline"></i> Logs
                    </a>
                  </li>
                @endcan
                @can('view_roles')
                  <li class="nav-item">
                    <a href="{{ route('roles.index') }}" class="nav-link {{ Request::is('systems/roles*') ? 'active' : '' }}"><i class="icon ion-ios-locked-outline"></i> Roles and Permissions
                    </a>
                  </li>
                @endcan
                <li class="nav-item">
                  <a href="{{ route('profile') }}" class="nav-link {{ Request::is('systems/profile*') ? 'active' : '' }}"><i class="icon ion-ios-person-outline "></i> Profile
                  </a>
                </li>
            </ul>
          </li><!-- nav-item -->
        @endcan
          @can('view_courses','view_sy')
          <li class="nav-item">
            <a href="" class="nav-link with-sub {{ Request::is('settings*') ? 'active' : '' }}">
              <i class="icon ion-ios-gear-outline"></i>
              <span>System Properties</span>
            </a>
            <ul class="nav-sub">
                @can('view_courses')
                  <li class="nav-item">
                    <a href="{{ route('courses.index') }}" class="nav-link {{ Request::is('settings/courses*') ? 'active' : '' }}"><i class="icon ion-grid"></i> Courses
                    </a>
                  </li>
                @endcan
                @can('view_sy')
                <li class="nav-item">
                  <a href="{{ route('sy.index') }}" class="nav-link {{ Request::is('settings/sy*') ? 'active' : '' }}"><i class="icon ion-ios-calendar-outline"></i> School Year
                  </a>
                </li>
                @endcan
            </ul>
          </li><!-- nav-item -->
        @endcan 
      </ul>
    </div><!-- kt-sideleft -->

    <!-- ##### HEAD PANEL ##### -->
    <div class="kt-headpanel">
      <div class="kt-headpanel-left">
        <a id="naviconMenu" href="" class="kt-navicon d-none d-lg-flex cbars naviconMenu_post">
          <img src="{{ asset('assets/images/bar.png') }}" width="30"/>
          <!-- <i class="fa fa-bars " style="font-size: 25px;"></i>  --> 
        </a>
        <a id="naviconMenuMobile" href="" class="kt-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
      </div><!-- kt-headpanel-left -->

      <div class="kt-headpanel-right">
        <div class="dropdown dropdown-notification">
          <a href="#" class="nav-link pd-x-7 pos-relative" title="Logs" data-toggle="dropdown">
              <i class="fa fa-file-o" style="font-size: 20px;"></i>
          </a>
          <div class="dropdown-menu wd-300 pd-0-force">
            <div class="dropdown-menu-header">
              <label>User's Logs</label>
            </div>
            <div class="media-list" >
              <div style="max-height: 250px;overflow: auto">
                <div class="table-responsive">
                  <table class="table table-striped table-hover" id="data-table">
                    <thead>
                      <tr>
                          <th>Action</th>
                          <th>Date/Time</th>
                      </tr>
                    </thead>
                      <tbody>
                        @foreach(App\SystemLog::orderBy('id','desc')->where('user_id',Auth::user()->id)->limit(100)->get() as $log)
                          <tr>
                            <td>{{$log->action}}</td>
                            <td>{{date('Y-m-d  h:i:s A',strtotime($log->created_at))}}</td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="dropdown dropdown-notification" style="display: none">
          <a href="" class="nav-link pd-x-7 pos-relative notifybtn" title="Notification" data-toggle="dropdown">
            <!-- <img src="../img/notification.svg" height="24"/> -->
            <i class="fa fa-bell-o" style="font-size: 20px;"></i>
            <!-- start: if statement -->
            <span class="nx square-8  pos-absolute t-15 r-0 rounded-circle"></span>
            <!-- end: if statement -->
          </a>
          <div class="dropdown-menu wd-300 pd-0-force">
            <div class="dropdown-menu-header">
              <label>Notifications</label>
              <a href="{{route('recent-mark')}}">Mark All as Read</a>
            </div><!-- d-flex -->

            <div class="media-list container-display" >
              <!-- loop starts here -->
              <div class="notification-display" style="max-height: 250px;overflow: auto">
                <!-- Display Notification here -->
              </div>
              <div class="media-list-footer">
                <a href="{{route('recent-list')}}" class="tx-12"><i class="fa fa-angle-down mg-r-5"></i> Show All Notifications</a>
              </div>
              <!-- loop ends here -->
            </div><!-- media-list -->
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
        <div class="dropdown dropdown-profile">
          <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
            @if(Auth::user()->photo)
              <img src="/assets/images/users/{{Auth::user()->photo}}" class="wd-32 rounded-circle" alt="">
            @else
              <img src="{{ asset('assets/images/default-profile.png') }}" class="wd-32 rounded-circle" alt="">
            @endif
            <span class="logged-name"><span class="hidden-xs-down">{{Auth::user()->username}}</span> <i class="fa fa-angle-down mg-l-3"></i></span>
          </a>
          <div class="dropdown-menu wd-200">
            <ul class="list-unstyled user-profile-nav">
              <li><a href="{{ route('profile') }}"><i class="icon ion-ios-person-outline"></i> Edit Profile</a></li>
              <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon ion-power"></i> Sign Out</a></li>
            </ul>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- kt-headpanel-right -->
    </div><!-- kt-headpanel -->
    <div class="kt-breadcrumb">
      <nav class="breadcrumb">
        <!-- <a class="breadcrumb-item" href="index.html">magen-iot-admin</a>
        <span class="breadcrumb-item active">Blank</span> -->
         @yield('content_header_link')
      </nav>
    </div><!-- kt-breadcrumb -->

    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>@yield('content_header')</h5>
      </div><!-- kt-pagetitle -->

      <div class="kt-pagebody">

        @yield('content')

      </div><!-- kt-pagebody -->
      <div class="kt-footer">
        <span>Copyright &copy; 2017. Bachelor of Science in Information Technology.</span>
        <span>Created by: Intent (Batch : 2017-2018).</span>
      </div><!-- kt-footer -->
    </div><!-- kt-mainpanel -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/lib/parsleyjs/parsley.js') }}"></script>

    <script src="{{ asset('assets/js/magen-iot-admin.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script type="text/javascript">
      $('.g-search').keyup(function(e){
        var ok = $(this).val();
        if(e.which == 13){
          window.location.href = "{{ route('search') }}?searchfield="+ok;
        }
      });
      $('.btnsearch').click(function(){
        var ok = $('.g-search').val();
        window.location.href = "{{ route('search') }}?searchfield="+ok;
      });
    </script>
    @stack('append_js')
  </body>
</html>
