<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            <ul id="themecolors" class="m-t-20">
                <li><b>With Light sidebar</b></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/default.css') }}" class="default-theme">1</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/green.css') }}" class="green-theme">2</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/red.css') }}" class="red-theme">3</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/blue.css') }}" class="blue-theme working">4</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/purple.css') }}" class="purple-theme">5</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/megna.css') }}" class="megna-theme">6</a></li>
                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/default-dark.css') }}" class="default-dark-theme">7</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/green-dark.css') }}" class="green-dark-theme">8</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/red-dark.css') }}" class="red-dark-theme">9</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/blue-dark.css') }}" class="blue-dark-theme">10</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/purple-dark.css') }}" class="purple-dark-theme">11</a></li>
                <li><a href="javascript:void(0)" data-theme="{{ asset('assets/material/css/colors/megna-dark.css') }}" class="megna-dark-theme ">12</a></li>
            </ul>
        </div>
    </div>
</div>