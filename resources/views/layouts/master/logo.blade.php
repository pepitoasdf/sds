<div class="navbar-header">
    <a class="navbar-brand" href="">
        <!-- Logo icon -->
        <b>
            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
            <!-- Dark Logo icon -->
            <img src="{{ asset('assets/images/logo-icon.png') }}" alt="SDS" class="dark-logo" />
            <!-- Light Logo icon -->
            <img src="{{ asset('assets/images/logo-light-icon.png') }}" alt="SDS" class="light-logo" />
        </b>
        <!--End Logo icon -->
        <!-- Logo text -->
        <span>
             <!-- dark Logo text -->
             <img src="{{ asset('assets/images/logo-text.png') }}" alt="SDS PIT-TC" class="dark-logo" />
             <!-- Light Logo text assets/images/logo-light-text.png -->    
             <!-- <img src="" class="light-logo" alt="SDS PIT-TC" style="color:#fff;font-weight: bold" /> -->
             <span class="light-logo" style="color:#fff;font-weight: bold" >
                 SDS PIT-TC
             </span>
         </span>
    </a>
</div>