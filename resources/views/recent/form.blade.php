<form action="" id="sdsform" method="POST" class="form-horizontal" enctype="multipart/form-data" autocomplete="off" data-parsley-validate>
    {{csrf_field()}}
    <hr class="mg-t-0">
    <fieldset>
        <legend>ID & Schoolyear</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">ID Number:</label>
                        <input type="text" class="form-control form-control-sm" disabled="disabled" name="id_number" placeholder="0000-00-000000" value="{{isset($student) ? $student->id_number : ''}}">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Schoolyear: <span class="tx-danger">*</span></label>
                        <select class="form-control form-control-sm sy" data-parsley-required-message="" required name="sy">
                            @foreach(App\Schoolyear::all() as $sy)
                                @if(isset($student))
                                    <option value="{{$sy->id}}" {{$sy->id == $student->sy_id ? 'selected' : ''}}>{{$sy->shortname}}</option>
                                @else
                                    <option value="{{$sy->id}}" {{$sy->id == $syactive->id ? 'selected' : ''}}>{{$sy->shortname}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Student Name, Gender</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">First Name: <span class="tx-danger">*</span></label>
                        <input class="form-control form-control-sm" type="text" name="firstname" value="{{isset($student) ? $student->student_firstname : ''}}" data-parsley-required-message="" placeholder="First Name" required="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Middle Name:</label>
                        <input type="text" name="middlename" value="{{isset($student) ? $student->student_middlename : ''}}" class="form-control form-control-sm" placeholder="Middle Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Last Name: <span class="tx-danger">*</span></label>
                        <input type="text" required class="form-control form-control-sm" value="{{isset($student) ? $student->student_lastname : ''}}" data-parsley-required-message="" placeholder="Last Name" name="lastname">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Sex: <span class="tx-danger">*</span></label>
                        <select class="form-control form-control-sm gender" data-parsley-required-message="" required name="gender">
                            <option value="">--Select--</option>
                            @if(isset($student))
                                <option value="Male" {{$student->sex == "Male" ? 'selected' : ''}}>Male</option>
                                <option value="Female" {{$student->sex == "Female" ? 'selected' : ''}}>Female</option>
                            @else
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Civil Status, Contact Number, Birth Date & Birth Place</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Civil Status: <span class="tx-danger">*</span></label>
                        <select class="form-control form-control-sm c_status" data-parsley-required-message=""  required name="c_status" tabindex="8" style="width: 100%;">
                            <option value="">--Select--</option>
                            @if(isset($student))
                                <option value="Single" {{$student->civil_status == "Single" ? 'selected' : ''}}>Single</option>
                                <option value="Married" {{$student->civil_status == "Married" ? 'selected' : ''}}>Married</option>
                                <option value="Widow" {{$student->civil_status == "Widow" ? 'selected' : ''}}>Widow</option>
                                <option value="Separated" {{$student->civil_status == "Separated" ? 'selected' : ''}}>Separated</option>
                            @else
                                <option value="Single">Single</option>
                                <option value="Married">Married</option>
                                <option value="Widow">Widow</option>
                                <option value="Separated">Separated</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Contact Number: <span class="tx-danger">*</span></label>
                        <input required  type="text" value="{{isset($student) ? $student->emergency_contact_number : ''}}" name="contact_number" data-parsley-required-message="" class="form-control form-control-sm" placeholder="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Date of Birth: <span class="tx-danger">*</span></label>
                        <input type="date" data-parsley-required-message="" class="form-control form-control-sm" required name="bday" placeholder="dd/mm/yyyy" value="{{isset($student) ? date('Y-m-d',strtotime($student->bithdate)) : ''}}">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Province: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" name="bp_province" required class="form-control form-control-sm province">
                            @if(isset($student))
                                <option value="{{$student->bithplace_province_id}}">{{$student->province($student->bithplace_province_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">City/Municipality: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" name="bp_city_mun" required class="form-control form-control-sm city_mun">
                            @if(isset($student))
                                <option value="{{$student->bithplace_city_id}}">{{$student->citymun($student->bithplace_city_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Barangay: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" name="bp_barangay" required class="form-control barangay form-control-sm">
                            @if(isset($student))
                                <option value="{{$student->bithplace_barangay_id}}">{{$student->barangay($student->bithplace_barangay_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Street: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" type="text" name="bp_street"  required value="{{isset($student) ? $student->bithplace_street : ''}}" class="form-control form-control-sm street" placeholder="Street">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Home:</label>
                        <input type="text" name="bp_home" value="{{isset($student) ? $student->bithplace_home : ''}}" class="form-control form-control-sm home"  placeholder="Home" >
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Course, Major, Year & Section</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Course: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" class="form-control form-control-sm" required name="course">
                            <option value="">--Select--</option>
                            @foreach(App\Course::all() as $courses)
                                @if(isset($student->course_id))
                                    <option value="{{$courses->id}}" {{$student->course_id == $courses->id ? 'selected' : ''}} >{{$courses->shortname}}</option>
                                @else
                                    <option value="{{$courses->id}}" {{$id == $courses->id ? 'selected' : ''}} >{{$courses->shortname}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Year: <span class="tx-danger">*</span></label>
                         <select data-parsley-required-message="" required class="form-control form-control-sm" name="year_course" >
                            <option value=""></option>
                            @foreach(App\Yearlevel::all() as $levels)
                                @if(isset($student->yearlevel_id))
                                    <option value="{{$levels->id}}" {{$student->yearlevel_id == $levels->id ? 'selected' : ''}} >{{$levels->shortname}}</option>
                                @else
                                    <option value="{{$levels->id}}" >{{$levels->shortname}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Section:</label>
                        <input type="text" class="form-control form-control-sm" placeholder="" value="{{isset($student) ? $student->section : ''}}"  name="section">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Major:</label>
                        <input type="text" value="{{isset($student->course_major) ? $student->course_major : ''}}" tabindex="61" class="form-control form-control-sm" name="course_major">
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Home Address</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Province: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required name="ha_province" class="form-control form-control-sm province">
                            @if(isset($student))
                                <option value="{{$student->home_address_province_id}}">{{$student->province($student->home_address_province_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">City/Municipality: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required  name="ha_city_mun" class="form-control form-control-sm city_mun">
                            @if(isset($student))
                                <option value="{{$student->home_address_city_id}}">{{$student->citymun($student->home_address_city_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Barangay: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required name="ha_barangay" class="form-control form-control-sm barangay">
                            @if(isset($student))
                                <option value="{{$student->home_address_barangay_id}}">{{$student->barangay($student->home_address_barangay_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Street: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->home_address_street : ''}}" name="ha_street" class="form-control form-control-sm street" placeholder="Street">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Home:</label>
                        <input type="text"  value="{{isset($student) ? $student->home_address_home : ''}}" name="ha_home" class="form-control form-control-sm home" placeholder="Home" >
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Tabango Address</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Province:</label>
                        <select name="ta_province" class="form-control form-control-sm province" >
                            @if(isset($student))
                                <option value="{{$student->ta_province_id}}">{{$student->province($student->ta_province_id)}}</option>
                            @else
                                <!-- <option value="837">LEYTE</option> -->
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">City/Municipality:</label>
                        <select  name="ta_city_mun" class="form-control form-control-sm city_mun">
                            @if(isset($student))
                                <option value="{{$student->ta_city_id}}">{{$student->citymun($student->ta_city_id)}}</option>
                            @else
                                 <!-- <option value="83745">TABANGO</option> -->
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Barangay: </label>
                        <select name="ta_barangay" class="form-control form-control-sm barangay">
                            @if(isset($student))
                                <option value="{{$student->ta_barangay_id}}">{{$student->barangay($student->ta_barangay_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Street: </label>
                        <input type="text" value="{{isset($student) ? $student->ta_street : ''}}" name="ta_street" class="form-control form-control-sm m-b-10 street" placeholder="Street">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Home:</label>
                        <input tabindex="25" type="text" value="{{isset($student) ? $student->ta_home : ''}}" name="ta_home" class="form-control form-control-sm m-b-10 home" placeholder="Home">
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Parents Infomation & No. Brother and Sister</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Father (First Name): <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->father_firstname : ''}}" name="f_firstname" class="form-control form-control-sm" placeholder="First Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Father (Middle Name):</label>
                        <input  type="text" value="{{isset($student) ? $student->father_middlename : ''}}" name="f_middlename" class="form-control form-control-sm" placeholder="Middle Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Father (Lastname): <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" type="text" value="{{isset($student) ? $student->father_firstname : ''}}" name="f_lastname" required class="form-control form-control-sm" placeholder="Last Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Father (Occupation): <span class="tx-danger">*</span></label>
                       <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->father_occupation : ''}}" name="f_occupation" class="form-control  form-control-sm" placeholder="Occupation">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Mother (First Name): <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->mother_firstname : ''}}" name="m_firstname" class="form-control form-control-sm" placeholder="First Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Mother (Middle Name):</label>
                        <input type="text" value="{{isset($student) ? $student->mother_middlename : ''}}" name="m_middlename" class="form-control form-control-sm" placeholder="Middle Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Mother (Lastname): <span class="tx-danger">*</span></label>
                        <input  data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->mother_lastname : ''}}" name="m_lastname" class="form-control form-control-sm" placeholder="Last Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Mother (Occupation): <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->mother_occupation : ''}}" name="m_occupation" class="form-control form-control-sm" placeholder="Occupation">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">No. of Brother & Sister.: <span class="tx-danger">*</span></label>
                        <select class="form-control form-control-sm" data-parsley-required-message="" name="siblings">
                            @for($i=0;$i<=20;$i++)
                                @if(isset($student))
                                    <option value="{{$i}}" {{ (trim($student->brother_sister_total) == $i) ? 'selected' : '' }}>{{$i}}</option>
                                @else
                                    <option value="{{$i}}">{{$i}}</option>
                                @endif
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>High School Graduate Information</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Name of School: <span class="tx-danger">*</span></label>
                        <textarea data-parsley-required-message=""  required rows="1" class="form-control form-control-sm" placeholder="Name of School" name="hs_school">{{isset($student) ? $student->highschool_graduate_school : ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Province: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required name="hs_province" class="form-control form-control-sm province">
                            @if(isset($student))
                                <option value="{{$student->highschool_graduate_province_id}}">{{$student->province($student->highschool_graduate_province_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">City/Municipality: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required name="hs_city_mun" class="form-control form-control-sm city_mun" >
                            @if(isset($student))
                                <option value="{{$student->highschool_graduate_city_id}}">{{$student->citymun($student->highschool_graduate_city_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Barangay: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required name="hs_barangay" class="form-control form-control-sm barangay">
                            @if(isset($student))
                                <option value="{{$student->highschool_graduate_barangay_id}}">{{$student->barangay($student->highschool_graduate_barangay_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Street: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->highschool_graduate_street : ''}}" name="hs_street" class="form-control form-control-sm street" placeholder="Street">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Year Graduated: <span class="tx-danger">*</span></label>
                        <select name="year_graduated" data-parsley-required-message="" required class="form-control form-control-sm">
                            <option value=""></option>
                            @for($i=2000;$i<=date('Y');$i++)
                                @if(isset($student))
                                    <option value="{{$i}}" {{ (trim($student->highschool_year_graduated) == $i) ? 'selected' : '' }}>{{$i}}</option>
                                @else
                                    <option value="{{$i}}">{{$i}}</option>
                                @endif
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Last School Attended (If students taken a colleges courses)</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Name of School:</label>
                        <textarea class="form-control form-control-sm" name="name_of_school" placeholder="Name of School" rows="1">{{isset($student) ? $student->lastschool_attended_school : ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Province: </label>
                        <select  name="ls_province" class="form-control form-control-sm province" >
                            @if(isset($student))
                                <option value="{{$student->lastschool_attended_province_id}}">{{$student->province($student->lastschool_attended_province_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">City/Municipality: </label>
                        <select  name="ls_city_mun" class="form-control form-control-sm city_mun" >
                            @if(isset($student))
                                <option value="{{$student->lastschool_attended_city_id}}">{{$student->citymun($student->lastschool_attended_city_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Barangay: </label>
                        <select  name="ls_barangay" class="form-control form-control-sm barangay">
                            @if(isset($student))
                                <option value="{{$student->lastschool_attended_barangay_id}}">{{$student->barangay($student->lastschool_attended_barangay_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Street: </label>
                        <input  type="text" value="{{isset($student) ? $student->lastschool_attended_street : ''}}" name="ls_street" class="form-control form-control-sm street" placeholder="Street">
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Nationality, Religion & Language Spoken at Home</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Nationality: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->nationality : ''}}" class="form-control form-control-sm" name="nationality" placeholder="Nationality">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Religion: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required  value="{{isset($student) ? $student->religion : ''}}" type="text" class="form-control form-control-sm" name="religion" placeholder="Religion">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Language Spoken at Home: <span class="tx-danger">*</span></label>
                        <textarea data-parsley-required-message="" required  class="form-control form-control-sm" placeholder="Language" rows="1" name="language_spoken">{{isset($student) ? $student->language_spoken : ''}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Emergency Contact Information</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">In case of emergency please notify: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->emergency_notify_name : ''}}" name="notify_name" class="form-control form-control-sm" placeholder="Name">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Relation: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->emergency_notify_relation : ''}}" name="notify_relation" class="form-control form-control-sm" placeholder="Relation">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Contact Number: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->emergency_contact_number : ''}}" name="contact_number" class="form-control form-control-sm" placeholder="Contact Number">
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Emergency Address</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Province: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required name="ea_province" class="form-control form-control-sm province" >
                            @if(isset($student))
                                <option value="{{$student->emergency_address_province_id}}">{{$student->province($student->emergency_address_province_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">City/Municipality: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required  name="ea_city_mun" class="form-control form-control-sm city_mun">
                            @if(isset($student))
                                <option value="{{$student->emergency_address_city_id}}">{{$student->citymun($student->emergency_address_city_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Barangay: <span class="tx-danger">*</span></label>
                        <select data-parsley-required-message="" required name="ea_barangay" class="form-control form-control-sm barangay">
                            @if(isset($student))
                                <option value="{{$student->emergency_address_barangay_id}}">{{$student->barangay($student->emergency_address_barangay_id)}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Street: <span class="tx-danger">*</span></label>
                        <input data-parsley-required-message="" required type="text" value="{{isset($student) ? $student->emergency_address_street : ''}}" name="ea_street" class="form-control form-control-sm street" placeholder="Street">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label">Home:</label>
                        <input  type="text" value="{{isset($student) ? $student->emergency_address_home : ''}}" name="ea_home" class="form-control form-control-sm home"  placeholder="Home">
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Profile</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Upload Photo: <span class="tx-danger">*</span></label>
                        @if(isset($student->user_photo))
                            <input  data-parsley-required-message="" name="photo" {{isset($student->user_photo) ? 'data-default-file=/assets/images/profile/'.$student->user_photo : '' }} accept="image/*" type="file" id="input-file-now-custom-2" class="dropify" data-height="300" />
                        @else
                            <input data-parsley-required-message=""  required="" name="photo" accept="image/*" type="file" id="input-file-now-custom-2" class="dropify" data-height="300" />
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Signature</legend>
        <div class="form-layout">
            <div class="row mg-b-5">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Signature:<span class="tx-danger">*</span></label>
                        <div id="signature-pad" class="m-signature-pad">
                            <div class="m-signature-pad--body">
                                <canvas></canvas>
                            </div>
                            <div class="m-signature-pad--footer">
                                <div class="description">Sign above</div>
                                <button type="button" class="button clear" data-action="clear">Clear</button>
                                <button type="button" class="button save" data-action="save">Capture</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Captured:</label>
                         @if(isset($student->signature))
                            <div class="capture-signature">
                                <img src="{{$student->signature}}" alt="capture-Signature" height="150px" />
                                <input type="hidden" name="signature-photo" value="{{$student->signature}}" class="signature-photo">
                            </div>
                        @else
                            <div class="capture-signature">
                                <img src="" alt="capture-Signature" width="90%" height="150px" />
                                <input type="hidden" name="signature-photo" class="signature-photo">
                            </div>
                        @endif
                        <br/>
                        <label class="form-control-label">Alternative Signature:</label>
                        <input type="file" class="form-control altsig" accept="image/*" name="altsig">
                        @if(isset($student->altsig))
                            <div class="capture-signature" style="width: 100%">
                                <img src="/assets/images/altsig/{{ $student->altsig }}" alt="alternative-Signature" height="150px" />
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    @if(isset($student))
        <fieldset>
            <legend>Update Status</legend>
            <div class="form-layout">
                <div class="row mg-b-5">
                    <div class="col-lg-3">
                        <div class="form-group">
                        <label class="form-control-label">Status: <span class="tx-danger">*</span></label>
                        <select required name="status" class="form-control form-control-sm" >
                            @if(isset($student))
                                <option value="Active" {{$student->status == "Active" ? "selected" : ""}}>Active</option>
                                <option value="Inactive" {{$student->status == "Inactive" ? "selected" : ""}}>Inactive</option>
                            @endif
                        </select>
                    </div>
                    </div>
                </div>
            </div>
        </fieldset>
    @endif
    <hr/>
    <div class="form-layout-footer">
        @can('accept_untrack')
            <button class="btn btn-default btnsubmit btn-m" type="submit" name="accept" value="accept"> Accept</button>
        @endcan
        @can('delete_untrack')
            <button class="btn btn-danger btnsubmit btn-m" type="submit" name="remove" value="remove"> Remove</button>
        @endcan
    </div>
</form>




@push('append_js')
    <script src="{{ asset('assets/lib/signature-pad/js/signature_pad.js') }}"></script>
    <script src="{{ asset('assets/lib/signature-pad/js/app.js') }}"></script>
    <script src="{{ asset('assets/lib/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $('.dropify').dropify();

        $(".province").select2({
            placeholder: 'Provinces',
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "/places/provinces",
                dataType : 'json',
                data : function (params) {
                    return {
                        name: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });

        $('.city_mun').select2({
            placeholder: 'City/Municipality',
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "/places/citymun",
                dataType : 'json',
                data : function (params) {
                    var prov = $(this).closest('div.row').find('.province').val();
                    return {
                        name: params.term,
                        ref_id: prov
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });

        $('.barangay').select2({
            placeholder: 'Barangay',
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "/places/barangays",
                dataType : 'json',
                data : function (params) {
                    var prov = $(this).closest('div.row').find('.province').val();
                    var cm = $(this).closest('div.row').find('.city_mun').val();
                    return {
                        name: params.term,
                        ref_id1: prov,
                        ref_id2: cm
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });
        $('form#sdsform').parsley().on('form:error', function (formInstance) {
           $('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#dc3545'});
        });

        $('.province').on('select2:select', function (evt) {
            $(this).closest('div').find('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#28a745'});
            $(this).closest('div.row').find('.city_mun').val(null).trigger('change.select2');
            $(this).closest('div.row').find('.barangay').val(null).trigger('change.select2');
        });
        $('.city_mun').on('select2:select', function (evt) {
            $(this).closest('div').find('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#28a745'});
            $(this).closest('div.row').find('.barangay').select2("val", null);
        });
        $('.barangay').on('select2:select', function (evt) {
            $(this).closest('div').find('.parsley-error + ul + .select2-container--default .select2-selection--single').css({'border-color' : '#28a745'});
        });
    </script>
@endpush

@push('append_css')
    <link href="{{ asset('assets/lib/dropify/dropify.min.css') }}" rel="stylesheet">
@endpush