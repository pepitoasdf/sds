@extends('layouts.master')
@section('title', ' - Untrack')
@section('content_header', 'Untrack Records')
@section('content_header_link')
	@can('view_untrack')
		<a class="breadcrumb-item" href="{{ route('recent-list') }}">List</a>
	@endcan
    <span class="breadcrumb-item active">Profile</span>
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">Student Information</h6>
        <p class="mg-b-10 mg-sm-b-10">All fields which are marked with asterisk(<span class="tx-danger">*</span>) are mandatory</p>
        @include('recent.form')
    </div>
@endsection
