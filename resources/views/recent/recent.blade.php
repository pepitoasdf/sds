@extends('layouts.master')
@section('title', ' - Untrack')
@section('content_header', 'Untrack Records')
@section('content_header_link')
    <span class="breadcrumb-item active">List</span>
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        @include('shared._alert')
        <h6 class="card-body-title">List</h6>
        <p class="mg-b-10 mg-sm-b-10">List of untracked records.</p>
        <div class="card-title mg-b-10">
            <span class="pull-left" style="padding-right: 10px;font-weight: normal;">
               <label style="line-height: 25px;">Mark All as :</label> 
            </span>
            <span class="pull-left">
                <select class="form-control form-control-sm mark" style="width: 100px;">
                   <option value="">--Select--</option>
                   <option value="Read">Read</option>
                   <option value="Unread">Unread</option>
                </select>
            </span>

            <span class="pull-right">
                <select class="form-control form-control-sm sy_filter" style="width: 100px;">
                    @foreach(App\Schoolyear::all() as $sy)
                        @if($fid)
                            <option value="{{$sy->id}}" {{$fid == $sy->id ? 'selected' : ''}}>{{$sy->shortname}}</option>
                        @else
                            <option value="{{$sy->id}}">{{$sy->shortname}}</option>
                        @endif
                    @endforeach
                </select>
            </span>
            <span class="pull-right" style="padding-right: 10px;font-weight: normal;">
               <label style="line-height: 25px;">Filter :</label> 
            </span>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th width="2%">
                            <input type="checkbox" class="all_check">
                        </th>
                        <th width="15%">ID No.</th>
                        <th colspan="3" width="32%">Student Name</th>
                        <th width="10%">Gender</th>
                        <th width="25%">Course</th>
                        <th width="8%">
                            Status
                        </th>
                        <th width="8%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($student as $s)
                        <tr>
                            <td>
                                <input type="checkbox" class="onlycheck"  form="checkmany" name="refid[]" value="{{$s->id}}">
                            </td>
                            <td>
                                @can('view_untrack')
                                    <a href="{{route('recent-view',$s->id)}}">
                                    {{$s->id_number}}
                                    </a>
                                @else
                                    {{$s->id_number}}
                                @endcan
                            </td>
                             <td width="12%">{{isset($s->student_lastname) ? $s->student_lastname : ''}}</td>
                            <td width="10%">{{isset($s->student_firstname) ? $s->student_firstname : ''}}</td>
                            <td width="3%">
                                @if(isset($s->student_middlename))
                                    {{ (strlen($s->student_middlename) > 0) ? $s->student_middlename[0].'.' : ''}}
                                @endif
                            </td>
                            <td>{{$s->sex}}</td>
                            <td>{{$s->getcourse->description}}</td>
                            <td class="marker">
                                    @if($s->seen == "Yes")
                                        <span class="text text-success"><strong>Read</strong></span>
                                    @else
                                        <span class="text text-default"><strong>Unread</strong></span>
                                    @endif
                            </td>
                            <td>
                                @can('accept_untrack')
                                    <form action="{{route('recent-update', $s->id)}}" style="display: inline" onsubmit="return confirm('Do you really want to Accept?')" method="POST">
                                        {{csrf_field()}}
                                        {{ method_field('PUT') }}
                                        <button type="submit" title="Accept?" class="btn btn-sm btn-info">
                                             <i class="fa fa-check"></i>
                                        </button>
                                    </form>
                                @endcan
                                @can('delete_untrack')
                                    <form action="{{route('recent-remove', $s->id)}}" style="display: inline" onsubmit="return confirm('Do you really want to delete?')" method="POST">
                                        {{csrf_field()}}
                                        {{ method_field('DELETE') }}
                                        <button type="submit"  title="Remove?" class="btn-delete btn btn-sm btn-danger">
                                             <i class="fa fa-trash-o"></i>
                                        </button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $student->links() }}

        <div class="mg-t-20">
        <form action="" method="post" id="checkmany" name="checkmany">
            {{csrf_field()}}
            @can('accept_untrack')
                <button class="btn btn-default btnsubmit btn-m" type="submit" name="accept" value="accept"> Accept</button>
            @endcan
            @can('delete_untrack')
                <button class="btn btn-danger btnsubmit btn-m" type="submit" name="remove" value="remove"> Remove</button>
            @endcan
        </form>
        </div>
    </div>
    @push('append_css')
        <link href="{{ asset('assets/lib/spectrum/spectrum.css') }}" rel="stylesheet">
    @endpush
    @push('append_js')
        <script src="{{ asset('assets/lib/spectrum/spectrum.js') }}"></script>
        <script type="text/javascript">
            var student = {{json_encode($student->pluck('id'))}};
            $('.sy_filter').change(function(e){
                e.preventDefault();
                var id = $(this).val();
                window.location.href = "?filter="+id;
            });

            $('.mark').change(function(){
                var mark = $(this).val();
                $.get('/untrack/mark',{id:student,mark:mark},function(data){
                    if(mark == "Read"){
                        $('table.table tbody tr').find('.marker').html('<span class="text text-success"><strong>Read</strong></span>');
                    }
                    else{
                        $('table.table tbody tr').find('.marker').html('<span class="text text-default"><strong><strong>Unread</strong></span>');
                    }
                });
            });

            $('.all_check').click(function(){ 
                var c = $(this).val();
                if($(this).is(':checked')){
                    $('table tbody tr').find('input[type="checkbox"]').prop('checked','checked');
                }
                else{
                    $('table tbody tr').find('input[type="checkbox"]').removeAttr('checked');
                }
            });

            $('.onlycheck').click(function(){
                var c = $('table tbody tr').find('.onlycheck').length;
                var t = $('table tbody tr').find('.onlycheck:checked').length;
                if(c == t){
                    $('.all_check').prop('checked','checked');
                }
                else{
                     $('.all_check').removeAttr('checked');
                }
            });
        </script>
    @endpush
@endsection
