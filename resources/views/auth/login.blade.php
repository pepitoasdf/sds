
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="vueghost">


    <title>SDS - PIT</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/pit-logo.png') }}">
    <!-- vendor css -->
    <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">

    <!-- magen-iot-admin CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/magen-iot-admin.css') }}">
  </head>

  <body style="background: url(/assets/images/bg.jpg); background-position: center;">

    <div class="signpanel-wrapper">
      <div class="signbox">
        <div class="signbox-header">
          <h4>Student Directory System</h4>
          <p class="mg-b-0"><img src="{{ asset('assets/images/pit-logo.png') }}" width="20px" height="20px"> PIT - Tabango Campus</p>
        </div><!-- signbox-header -->
        <div class="signbox-body">
            @if($errors->has('username') || $errors->has('password'))
                <div class="alert alert-danger mg-b-0" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Invalid</strong> Username or Password.
                </div>
            @endif
            <!-- alert -->
            <form  method="post" id="loginform" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="form-control-label">Username:</label>
                    <input type="text" required="" name="username" value="{{ old('username') }}" autofocus="" placeholder="User name" class="form-control form-control-sm {{ $errors->has('username') ? ' is-invalid' : '' }}">
                </div>
                <!-- form-group -->
                <div class="form-group">
                    <label class="form-control-label">Password:</label>
                    <input type="password" required="" name="password" placeholder="Password" class="form-control form-control-sm {{ $errors->has('username') ? ' is-invalid' : '' }}">
                </div><!-- form-group -->
                <button type="submit" class="btn btn-dark btn-block">Sign In</button>
                <div class="tx-center bd pd-10 mg-t-40">Don't have an account? <a href="{{ url('/register') }}">Create an account</a></div>
            </form>
        </div><!-- signbox-body -->
      </div><!-- signbox -->
    </div><!-- signpanel-wrapper -->

    <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>
  </body>
</html>
