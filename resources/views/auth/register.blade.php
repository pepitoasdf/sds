
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="vueghost">


    <title>SDS - PIT</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/pit-logo.png') }}">
    <!-- vendor css -->
    <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">

    <!-- magen-iot-admin CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/magen-iot-admin.css') }}">
  </head>

  <body style="background: url(/assets/images/bg.jpg); background-position: center;">

    <div class="signpanel-wrapper">
      <div class="signbox signup">
        <div class="signbox-header">
          <h4>Student Directory System</h4>
          <p class="mg-b-0">PIT - Tabango Campus</p>
        </div><!-- signbox-header -->
        <div class="signbox-body">
            @if(App\GuestPermission::get_guest()->register == 'Yes')
            <form id="loginform" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control form-control-sm {{ $errors->has('firstname') ? ' is-invalid' : '' }}" type="text" name="firstname" required="" value="{{ old('firstname') }}" placeholder="First Name">
                        @if ($errors->has('firstname'))
                            <span class="help-block text text-danger">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control form-control-sm {{ $errors->has('lastname') ? ' is-invalid' : '' }}" type="text" name="lastname" required="" value="{{ old('lastname') }}" placeholder="Last Name">
                        @if ($errors->has('lastname'))
                            <span class="help-block text text-danger">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control form-control-sm {{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" name="username" required="" value="{{ old('username') }}" placeholder="Username">
                        @if ($errors->has('username'))
                            <span class="help-block text text-danger">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control form-control-sm {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required="" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block text text-danger text text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control form-control-sm {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" type="password" name="password_confirmation" required="" placeholder="Confirm Password">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block text text-danger">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-dark btn-block">Sign Up</button>
            </form>
            <div class="tx-center bd pd-10 mg-t-40">Already have an account? <a href="{{ url('/login') }}">Sign In</a></div>
            @else
                <div class="alert alert-info" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="d-flex align-items-center justify-content-start">
                        <i class="icon ion-ios-information alert-icon tx-24 mg-t-5 mg-xs-t-0"></i>
                        <span><strong>Heads up!</strong> Registration form for new users currently not available </span>
                    </div><!-- d-flex -->
                </div><!-- alert -->
            @endif
        </div><!-- signbox-body -->
      </div><!-- signbox -->
    </div><!-- signpanel-wrapper -->

    <script src="{{ asset('assets/lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/bootstrap.js') }}"></script>
  </body>
</html>
