@extends('layouts.master')
@section('title', ' - Courses')
@section('content_header', 'Add Courses')
@section('content_header_link')
    @can('view_courses')
        <a class="breadcrumb-item" href="{{ route('courses.index') }}">List</a>
    @endcan
    @can('add_courses')
        <span class="breadcrumb-item active">Add Courses</span>
    @endcan
@endsection
@section('content')
<div class="card pd-20 pd-sm-40" style="min-height: 400px;">
    <form class="form-horizontal" action="{{route('courses.store')}}" method="POST">
        @include('courses._form')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <label class="control-label text-right col-sm-4"></label>
                    <div class="col-md-8">
                        @can('add_courses')
                            <button type="submit" class="btn btn-default btn-sm"> Save</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection