<<!DOCTYPE html>
<html>
<head>
	<title>Courses's List</title>
</head>
<style type="text/css">
	table{
		width: 100%;
	}
	th,td{
		padding-left: 7px;
		padding-right: 7px;
	}
	h2{
		text-align: center;
	}
</style>
<body>
	<div>
		<h2>List Of Courses</h2>
	</div>
	<br/>
	<table class="table table-striped table-hover" id="data-table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Abbreviation</th>
                    <th>Description</th>
                    <th>Created At</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->id }}</td>
                    <td>{{ $d->shortname }}</td>
                    <td>{{ $d->description }}</td>
                    <td>{{ $d->created_at->toFormattedDateString() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
</body>
</html>