@extends('layouts.master')
@section('title', ' - Courses')
@section('content_header', 'Courses')
@section('content_header_link')
    <span class="breadcrumb-item active">List</span>
    @can('add_courses')
        <a class="breadcrumb-item" href="{{ route('courses.create') }}">Add Course</a>
    @endcan
@endsection
@section('content')
    <div class="card pd-20 pd-sm-40" style="min-height: 450px;">
        @include('shared._alert')
        <h6 class="card-body-title">List</h6>
        <p class="mg-b-10 mg-sm-b-10">List of courses.</p>
        <div class="card-title mg-b-10">
            @can('print_courses')
                <a href="{{route('courses-print')}}" target="_blank" class="btn btn-info btn-sm pull-left">Print</a>
            @endcan
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="data-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Abbreviation</th>
                    <th>Description</th>
                    <th>Created At</th>
                    @can('edit_courses', 'delete_courses')
                    <th class="text-center">Actions</th>
                    @endcan
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->shortname }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ $item->created_at->toFormattedDateString() }}</td>
                        @can('edit_courses')
                        <td class="text-center">
                            @include('shared._actions', [
                                'entity' => 'courses',
                                'id' => $item->id
                            ])
                        </td>
                        @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $data->links() }}
    </div>
@endsection