<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Province;
use App\City;
use App\Barangay;
use App\SyCourse;
use App\Schoolyear;
class StudentHeader extends Model
{
    protected $guarded = ['id'];

    public function getsy(){
        return $this->belongsTo(Schoolyear::class,'sy_id');
    }

    public function getcourse(){
        return $this->belongsTo(Course::class,'course_id');
    }

    public function yearlevel(){
        return $this->belongsTo(Yearlevel::class,'yearlevel_id');
    }

    public function province($id){
        $refid = str_pad($id,4,"0",STR_PAD_LEFT);
        $data =  Province::where('provCode',$refid)->first();
        return ($data) ? $data->provDesc : '';
    }

    public function citymun($id){
        $refid = str_pad($id,6,"0",STR_PAD_LEFT);
        $data =  City::where('citymunCode',$refid)->first();
        return ($data) ? $data->citymunDesc : '';
    }

    public function barangay($id){
        $data =  Barangay::where('id',$id)->first();
        return ($data) ? $data->brgyDesc : '';
    }

    public function getStudentFirstnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getStudentLastnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getStudentMiddlenameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getFatherFirstnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getFatherMiddlenameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getFatherLastnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getMotherFirstnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getMotherMiddlenameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getMotherLastnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function gethome_address(){
        $a = ucfirst(strtolower($this->province($this->home_address_province_id)));
        $b = ucfirst(strtolower($this->citymun($this->home_address_city_id)));
        $c = ucfirst(strtolower($this->barangay($this->home_address_barangay_id)));
        $d = ucfirst(strtolower($this->home_address_home));
        $e = ucfirst(strtolower($this->home_address_street));
        
        $data = [$d,$e,$c,$b,$a];
        $holder = '';
        $x = 0;
        foreach ($data as $key => $value) {
            if($value){
                if($x == 0){
                    $holder = $value;
                }
                else{
                     $holder .= ', '.$value;
                }
                $x++;
            }
        }
        return $holder;
    }

    public function getbirth_place(){
        $a = ucfirst(strtolower($this->province($this->bithplace_province_id)));
        $b = ucfirst(strtolower($this->citymun($this->bithplace_city_id)));
        $c = ucfirst(strtolower($this->barangay($this->bithplace_barangay_id)));
        $d = ucfirst(strtolower($this->bithplace_home));
        $e = ucfirst(strtolower($this->bithplace_street));
        
        $data = [$d,$e,$c,$b,$a];
        $holder = '';
        $x = 0;
        foreach ($data as $key => $value) {
            if($value){
                if($x == 0){
                    $holder = $value;
                }
                else{
                     $holder .= ', '.$value;
                }
                $x++;
            }
        }
        return $holder;
    }

    public function gettabango_address(){
        $a = ucfirst(strtolower($this->province($this->ta_province_id)));
        $b = ucfirst(strtolower($this->citymun($this->ta_city_id)));
        $c = ucfirst(strtolower($this->barangay($this->ta_barangay_id)));
        $d = ucfirst(strtolower($this->ta_home));
        $e = ucfirst(strtolower($this->ta_street));
        
        $data = [$d,$e,$c,$b,$a];
        $holder = '';
        $x = 0;
        foreach ($data as $key => $value) {
            if($value){
                if($x == 0){
                    $holder = $value;
                }
                else{
                     $holder .= ', '.$value;
                }
                $x++;
            }
        }
        return $holder;
    }

    public function gethighschool_address(){
        $a = ucfirst(strtolower($this->province($this->highschool_graduate_province_id)));
        $b = ucfirst(strtolower($this->citymun($this->highschool_graduate_city_id)));
        $c = ucfirst(strtolower($this->barangay($this->highschool_graduate_barangay_id)));
        $e = ucfirst(strtolower($this->highschool_graduate_street));
        
        $data = [$e,$c,$b,$a];
        $holder = '';
        $x = 0;
        foreach ($data as $key => $value) {
            if($value){
                if($x == 0){
                    $holder = $value;
                }
                else{
                     $holder .= ', '.$value;
                }
                $x++;
            }
        }
        return $holder;
    }
    public function get_lastschool_address(){
        $a = ucfirst(strtolower($this->province($this->lastschool_attended_province_id)));
        $b = ucfirst(strtolower($this->citymun($this->lastschool_attended_city_id)));
        $c = ucfirst(strtolower($this->barangay($this->lastschool_attended_barangay_id)));
        $e = ucfirst(strtolower($this->lastschool_attended_street));
        
        $data = [$e,$c,$b,$a];
        $holder = '';
        $x = 0;
        foreach ($data as $key => $value) {
            if($value){
                if($x == 0){
                    $holder = $value;
                }
                else{
                     $holder .= ', '.$value;
                }
                $x++;
            }
        }
        return $holder;
    }

    public function get_emergency_address(){
        $a = ucfirst(strtolower($this->province($this->emergency_address_province_id)));
        $b = ucfirst(strtolower($this->citymun($this->emergency_address_city_id)));
        $c = ucfirst(strtolower($this->barangay($this->emergency_address_barangay_id)));
        $d = ucfirst(strtolower($this->emergency_address_home));
        $e = ucfirst(strtolower($this->emergency_address_street));
        
        $data = [$d,$e,$c,$b,$a];
        $holder = '';
        $x = 0;
        foreach ($data as $key => $value) {
            if($value){
                if($x == 0){
                    $holder = $value;
                }
                else{
                     $holder .= ', '.$value;
                }
                $x++;
            }
        }
        return $holder;
    }

    public function processRequest($request, $count = 0, $accept = 'No'){
        $count = $count + 1000;
        $sycourse = Schoolyear::where('status','Applied')->first();

        $sy = str_pad($sycourse->shortname, 9,"0",STR_PAD_LEFT);
        $f4 = $sy[2].$sy[3].$sy[7].$sy[8];
        $f2 = str_pad(($request->input('course')) ? $request->input('course') : 0, 2,"0",STR_PAD_LEFT);

        if($request->hasFile('photo')){
            $photoName = time().'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('assets/images/profile'), $photoName);
            $this->user_photo = $photoName;
        }
        if($request->hasFile('altsig')){
            $photoName = time().'.'.$request->altsig->getClientOriginalExtension();
            $request->altsig->move(public_path('assets/images/altsig'), $photoName);
            $this->altsig = $photoName;
        }
    	$this->id_number = $f4.'-'.$f2.'-'.date('mdy',strtotime($request->input('bday')));
    	$this->course_id = ($request->input('course')) ? $request->input('course') : 0;
    	$this->yearlevel_id = ($request->input('year_course')) ? $request->input('year_course') : 0;
        $this->sy_id = ($request->input('sy')) ? $request->input('sy') : 0;
    	$this->section = $request->input('section');
    	$this->student_firstname = $request->input('firstname');
    	$this->student_lastname = $request->input('lastname');
    	$this->student_middlename = $request->input('middlename');
    	$this->sex = $request->input('gender');
    	$this->civil_status = $request->input('c_status');
    	$this->bithdate = date('Y-m-d',strtotime($request->input('bday')));
    	$this->bithplace_province_id = $request->input('bp_province');
    	$this->bithplace_city_id = $request->input('bp_city_mun');
    	$this->bithplace_barangay_id = $request->input('bp_barangay');
    	$this->bithplace_street = $request->input('bp_street');
    	$this->bithplace_home = $request->input('bp_home');
    	$this->brother_sister_total = $request->input('siblings');
    	$this->home_address_province_id = $request->input('ha_province');
    	$this->home_address_city_id = $request->input('ha_city_mun');
    	$this->home_address_barangay_id = $request->input('ha_barangay');
    	$this->home_address_street = $request->input('ha_street');
    	$this->home_address_home = $request->input('ha_home');
    	$this->ta_province_id = $request->input('ta_province');
    	$this->ta_city_id = $request->input('ta_city_mun');
    	$this->ta_barangay_id = $request->input('ta_barangay');
    	$this->ta_street = $request->input('ta_street');
    	$this->ta_home = $request->input('ta_home');
    	$this->father_firstname = $request->input('f_firstname');
    	$this->father_lastname = $request->input('f_middlename');
    	$this->father_middlename = $request->input('f_lastname');
    	$this->father_occupation = $request->input('f_occupation');
    	$this->mother_firstname = $request->input('m_firstname');
    	$this->mother_lastname = $request->input('m_middlename');
    	$this->mother_middlename = $request->input('m_lastname');
    	$this->mother_occupation = $request->input('m_occupation');
    	$this->highschool_graduate_school = $request->input('hs_school');
    	$this->highschool_graduate_province_id = $request->input('hs_province');
    	$this->highschool_graduate_city_id = $request->input('hs_city_mun');
    	$this->highschool_graduate_barangay_id = $request->input('hs_barangay');
    	$this->highschool_graduate_street = $request->input('hs_street');
    	$this->highschool_year_graduated = $request->input('year_graduated');
    	$this->lastschool_attended_school = $request->input('name_of_school');
    	$this->lastschool_attended_province_id = $request->input('ls_province');
    	$this->lastschool_attended_city_id = $request->input('ls_city_mun');
    	$this->lastschool_attended_barangay_id = $request->input('ls_barangay');
    	$this->lastschool_attended_street = $request->input('ls_street');
    	$this->nationality = $request->input('nationality');
    	$this->religion = $request->input('religion');
    	$this->language_spoken = $request->input('language_spoken');
    	$this->emergency_notify_name = $request->input('notify_name');
    	$this->emergency_notify_relation = $request->input('notify_relation');
    	$this->emergency_address_province_id = $request->input('ea_province');
    	$this->emergency_address_city_id = $request->input('ea_city_mun');
    	$this->emergency_address_barangay_id = $request->input('ea_barangay');
    	$this->emergency_address_street = $request->input('ea_street');
    	$this->emergency_address_home = $request->input('ea_home');
    	$this->emergency_contact_number = $request->input('contact_number');
    	$this->course_major = ($request->input('course_major')) ? $request->input('course_major') : '';
    	$this->signature = $request->input('signature-photo');
    	$this->contact_number = $request->input('mycontact_no');
    	$this->status = ($request->input('status')) ? $request->input('status') : 'Active';
        $this->is_accepted = $accept;
    	return $this;
    }
}
