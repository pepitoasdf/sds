<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemLog extends Model
{
    protected $guarded = ['id'];

    public function getusers(){
    	return $this->belongsTo(User::class,'user_id');
    }
}
