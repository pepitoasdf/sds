<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Schoolyear extends Model
{
	protected $guarded = ['id'];
	public static function get_effective(){
		return static::where('effective_date','<=',date('Y-m-d'))
			->whereNotNull('effective_date')->first();
	}
}
