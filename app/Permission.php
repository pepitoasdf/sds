<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TabMenu;
class Permission extends Model
{
    public static function defaultPermissions()
    {
        return [
            'view_dashboard',

            'view_students',
            'add_students',
            'edit_students',
            'print_students',

            'view_untrack',
            'accept_untrack',
            'delete_untrack',

            'view_users',
            'add_users',
            'edit_users',
            'print_users',
            'delete_users',

            'view_logs',
            'print_logs',

            'view_courses',
            'add_courses',
            'edit_courses',
            'print_courses',
            'delete_courses',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_reports',
            'print_reports',

            'view_sy',
            'edit_sy',
            'add_sy'
        ];
    }
}
