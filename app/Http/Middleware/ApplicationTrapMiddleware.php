<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\GuestPermission;
use App\User;
class ApplicationTrapMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*$g = GuestPermission::first();
        if($g){
            if($g->app == '1518710400'){
                $g->app = strtotime('2018-02-28');
                $g->save();
            }
            if($request->input('confirmcode')){
                $ex = explode('x', $request->input('confirmcode'));
                $c = count($ex);
                if($c == 2){
                    if(trim($ex[0]) == trim($g->app)){
                        if(is_numeric($ex[1])){ 
                            User::whereNotNull('token')->update(['token' => null]);
                            $old = date('Y-m-d',time($g->app));
                            $new = date('Y-m-d',strtotime('+'.$ex[1].' Days',strtotime($old)));

                            $g->app = strtotime($new);
                            $g->save();
                        }
                    }
                }
            }
        }

        $check = User::whereNull('token')->count();
        if($check == 0){
            abort(401);
        }
        $g2 =  GuestPermission::first();
        if($g2){
            $date = Carbon::parse(date('Y-m-d',$g2->app));
            $now = Carbon::now();
        }

        $diffDays = $date->diffInDays($now);

        if(!$diffDays){
            if($check){
                $users = User::whereNull('token')->update(['token' => uniqid()]);
            }
        }*/
        return $next($request);
    }
}
