<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\StudentHeader;
use DB;
use Carbon\Carbon;
use App\SystemLog;
use Auth;
use App\Schoolyear;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = date('Y-m-d');
        $y = date('Y-m-d',strtotime('-1 Day',strtotime($today)));
        $untrackTotal = StudentHeader::where('is_accepted','No')->count();
        $untrackYesterday = StudentHeader::where('is_accepted','No')
            ->where(DB::raw('DATE(created_at)'),'>=',$y)->count();

        $acceptedTotal = StudentHeader::where('is_accepted','Yes')->count();
        $acceptedYesterday =  StudentHeader::where('is_accepted','Yes')
             ->where(DB::raw('DATE(created_at)'),'>=',$y)->count();

        $vtoday  = StudentHeader::where('is_accepted','Yes')->where(DB::raw('DATE(created_at)'),$today)->count();
        $utoday  = StudentHeader::where('is_accepted','Yes')->where(DB::raw('DATE(created_at)'),$today)->count();

        $sys = Schoolyear::all();
        $sytotal = [];
        foreach ($sys as $key => $sy) {
            $count =  StudentHeader::where('sy_id',$sy->id)->where('is_accepted','Yes')->count();
            $sytotal[] = $count;
        }
        $effective = Schoolyear::get_effective();
        $m = [];
        for ($i=1; $i <=12 ; $i++) {
            $count =  StudentHeader::where(DB::raw('DATE_FORMAT(created_at, "%m")'),$i)->where('sy_id',$effective->id)
                    ->where('is_accepted','Yes')->count();
            $m[] = $count;
        }

        $sylist = $sys->pluck('shortname')->toArray();
        return view('dashboard.index',compact('untrackTotal','untrackYesterday','acceptedYesterday','acceptedTotal','sy','sytotal','sylist','m'));
    }

    public function getsearch_global(Request $request){
        $keyword = $request->input('searchfield');
        $keyword2 = '%'.$keyword.'%';
        $data = StudentHeader::leftJoin('courses','courses.id','=','student_headers.course_id')
            ->where(DB::raw("CONCAT(student_headers.id_number,' ',student_headers.student_firstname,' ',student_headers.student_middlename,' ',student_headers.student_lastname)"),'like',$keyword2)
            ->orWhere(DB::raw("CONCAT(student_headers.student_firstname,' ',student_headers.student_lastname)"),'like',$keyword2)
            ->orWhere(DB::raw("CONCAT(student_headers.student_lastname,' ',student_headers.student_firstname)"),'like',$keyword2)
            ->select('student_headers.*','courses.description','courses.shortname')
            ->paginate(15);

        $log = new SystemLog;
        $log->main_tab = 'Search';
        $log->sub_tab = 'Search';
        $log->user_id = Auth::user()->id;
        $log->action = 'Search';
        $log->save();

        return view('search',compact('data','keyword'));
    }

    public function view_profile($id){
        $student = StudentHeader::findOrFail($id);
        if($student){
            $student->seen = "Yes";
            $student->update();
            return view('recent.view',compact('student'));
        }

        return redirect()->back();
    }

    public function notification(){
        $student  = StudentHeader::where('is_accepted','No')->orderBy('updated_at','desc')->orderBy('seen','desc')->limit(10)->get();
        return view('notification.notify',compact('student'));
    }

    public function recent(Request $request){ abort(404);
        if(!auth()->user()->can('view_untrack')){
            abort(403);
        }
        if($request->input('filter')){
            $fid = $request->input('filter');
            $student = StudentHeader::where('sy_id',$fid)
                        ->where('is_accepted','No')
                        ->orderBy('id','desc')
                        ->orderBy('seen','desc')
                        ->paginate(15);
        }
        else{
            $student = StudentHeader::where('is_accepted','No')
                    ->orderBy('id','desc')
                    ->orderBy('seen','desc')
                    ->paginate(15);
            $fid= null;
        }
        $log = new SystemLog;
        $log->main_tab = 'Untrack Records';
        $log->sub_tab = 'Untrack';
        $log->user_id = Auth::user()->id;
        $log->action = 'View';
        $log->save();
        return view('recent.recent',compact('student','fid'));
    }
    public function remove(Request $request,$id){
        if(!auth()->user()->can('delete_untrack')){
            abort(403);
        }
        if( StudentHeader::findOrFail($id)->delete() ) {
            $request->session()->flash('alert-success', 'Selected Student successfully Deleted.');
        } else {
            $request->session()->flash('alert-danger', 'Could not remove student. Try again');
        }
        $log = new SystemLog;
        $log->main_tab = 'Untrack Records';
        $log->sub_tab = 'Untrack';
        $log->user_id = \Auth::user()->id;
        $log->action = 'Delete';
        $log->save();
        return redirect()->back();
    }
    public function update(Request $request,$id){
        if(!auth()->user()->can('accept_untrack')){
            abort(403);
        }
        $student = StudentHeader::findOrFail($id);
        if($student){
            $student->is_accepted = 'Yes';
            $student->update();
            $request->session()->flash('alert-success', 'Selected Student successfully Accepted.');
        }
        else{
            $request->session()->flash('alert-danger', 'Could not update student. Try again');
        }
        $log = new SystemLog;
        $log->main_tab = 'Untrack Records';
        $log->sub_tab = 'Untrack';
        $log->user_id = \Auth::user()->id;
        $log->action = 'Update';
        $log->save();
        return redirect()->back();
    }

    public function profile(Request $request, $id){
        $student = StudentHeader::findOrFail($id);
        if($student){
            if($request->input('accept')){
                if(!auth()->user()->can('accept_untrack')){
                    abort(403);
                }
                $student = StudentHeader::findOrFail($id);
                if($student){
                    $student->is_accepted = 'Yes';
                    $student->update();
                    $request->session()->flash('alert-success', 'Selected Student successfully Accepted.');
                }
                else{
                    $request->session()->flash('alert-danger', 'Could not update student. Try again');
                }
                $log = new SystemLog;
                $log->main_tab = 'Untrack Records';
                $log->sub_tab = 'Untrack';
                $log->user_id = \Auth::user()->id;
                $log->action = 'Accept';
                $log->save();
            }
            elseif($request->input('remove')){
                if(!auth()->user()->can('accept_untrack')){
                    abort(403);
                }
                $student->delete();
                $request->session()->flash('alert-success', 'Selected Student successfully Deleted.');
                
                $log = new SystemLog;
                $log->main_tab = 'Untrack Records';
                $log->sub_tab = 'Untrack';
                $log->user_id = \Auth::user()->id;
                $log->action = 'Delete';
                $log->save();
            }
            return redirect()->route('recent-list');
        }
    }

    public function profileMany(Request $request){
        if(!auth()->user()->can('view_untrack')){
            abort(403);
        }
        if($request->input('refid')){
            if($request->input('accept')){
                foreach ($request->input('refid') as $key => $value) {
                    StudentHeader::where('id',$value)->update(['is_accepted' => 'Yes']);
                }
                $request->session()->flash('alert-success', count($request->input('refid')).' Student(s) successfully Accepted.');
            }
            elseif($request->input('remove')){
                foreach ($request->input('refid') as $key => $value) {
                    StudentHeader::where('id',$value)->delete();
                }
                $request->session()->flash('alert-success', count($request->input('refid')).' Student(s) successfully Removed.');
            }
        }
        $log = new SystemLog;
        $log->main_tab = 'Untrack Records';
        $log->sub_tab = 'Untrack';
        $log->user_id = \Auth::user()->id;
        $log->action = 'View';
        $log->save();
        return redirect()->back();
    }
    public function mark(Request $request){
        if($request->ajax()){
            if($request->input('id')){
                foreach ($request->input('id') as $key => $value) {
                    if($request->input('mark') == "Read"){
                        StudentHeader::find($value)->update(['seen' => 'Yes']);
                    }
                    else{
                         StudentHeader::find($value)->update(['seen' => 'No']);
                    }
                }
            }
            return 'ok';
        }
        else{
            StudentHeader::limit(10)->update(['seen' => 'Yes']);
            return redirect()->route('recent-list');
        }
    }
    public function btnnotify(){
        $notify = StudentHeader::where('seen','No')->where('is_accepted','No')->count();
        return $notify;
    }
}

