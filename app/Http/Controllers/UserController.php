<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Hash;
use App\SystemLog;
class UserController extends Controller
{
    use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->page_num = 15;
    }

    public function index()
    {
        $result = User::latest()->paginate($this->page_num);
        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'View';
        $log->save();
        return view('user.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::select('name', 'id')->get();
        
        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'Create New';
        $log->save();

        return view('user.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|min:1',
            'lastname' => 'required|min:1',
            'username' => 'required|unique:users',
            'password' => 'required|min:4',
            'roles' => 'required|min:1'
        ]);

        // hash password
        $request->merge(['password' => bcrypt($request->get('password'))]);

        // Create the user
        if ( $user = User::create($request->except('roles', 'permissions')) ) {

            $this->syncPermissions($request, $user);

            $request->session()->flash('alert-success', 'User has been created.');

        } else {
            $request->session()->flash('alert-danger', 'Unable to create user.');
        }

        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'Store';
        $log->save();

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all('name', 'id');
        $permissions = Permission::all('name', 'id');
        $user_role = $user->roles->pluck('id')->toArray();

        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'Edit';
        $log->save();

        return view('user.edit', compact('user', 'roles', 'permissions','user_role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required|min:1',
            'lastname' => 'required|min:1',
            'username' => 'required|unique:users,username,' . $id,
            'roles' => 'required|min:1',
            'password' => 'required|string|min:4|confirmed'
        ]);

        // Get the user
        $user = User::findOrFail($id);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password','oldpassword'));

        // check for password change
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        // Handle the user roles
        $this->syncPermissions($request, $user);

        $user->save();

        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'Update';
        $log->save();

        $request->session()->flash('alert-success', 'User has been updated.');

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy(Request $request,$id)
    {
        if ( Auth::user()->id == $id ) {
            $request->session()->flash('alert-warning', 'Deletion of currently logged in user is not allowed :(');
            return redirect()->back();
        }

        if( User::findOrFail($id)->delete() ) {
            $request->session()->flash('alert-success', 'User has been deleted.');
        } else {
            $request->session()->flash('alert-warning', 'User not deleted.');
        }

        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'Delete';
        $log->save();

        return redirect()->back();
    }

    /**
     * Sync roles and permissions
     *
     * @param Request $request
     * @param $user
     * @return string
     */
    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }

    public function printall(){
        if(!auth()->user()->can('print_users')){
            abort(403);
        }
        $users = User::all();

        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'Print All';
        $log->save();

        $pdf = PDF::setPaper("letter");
        $pdf->loadView('user.print',compact('users'));
        return $pdf->stream();
    }

    public function profile(){
        $user = auth()->user();
        
        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'View Profile';
        $log->save();

        return view('profile',compact('user'));
    }

    public function updateProfile(Request $request){
        $u = auth()->user();
        $user = User::findOrFail($u->id);

        if($request->input('update_photo')){
            if($request->hasFile('photo')){
                $photoName = time().'.'.$request->photo->getClientOriginalExtension();
                $request->photo->move(public_path('assets/images/users'), $photoName);
                $user->photo = $photoName;
                $user->save();
                $request->session()->flash('alert-success', 'User has been Updated.');
            }
        }
        else{
            if($request->hasFile('photo')){
                $photoName = time().'.'.$request->photo->getClientOriginalExtension();
                $request->photo->move(public_path('assets/images/users'), $photoName);
                $user->photo = $photoName;
            }

            if(Hash::check($request->input('oldpassword'), $user->password)){
                $user->username = $request->input('username');
                $user->password = bcrypt($request->input('newpassword'));
                $user->firstname = $request->input('firstname');
                $user->middlename = $request->input('middlename');
                $user->lastname = $request->input('lastname');
                $user->contact_number = $request->input('contact_number');
                $user->save();
                $request->session()->flash('alert-success', 'User has been Updated.');
            }
            else{
                $request->session()->flash('alert-danger', 'Something went wrong. Try again');
            }
        }
        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Users';
        $log->user_id = Auth::user()->id;
        $log->action = 'Update';
        $log->save();

        return redirect()->back();
    }

    public function logs_index(Request $request){
        if($request->input('filter')){
            $logs = SystemLog::where('user_id',$request->input('filter'))->orderBy('id','desc')->paginate(15);
        }
        else{
             $logs = SystemLog::orderBy('id','desc')->paginate(15);
        }
        $fid = $request->input('filter');
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'Logs';
        $log->user_id = Auth::user()->id;
        $log->action = 'View';
        $log->save();
        return view('logs.index',compact('logs','fid'));
    }

    public function log_print(){
        $data = SystemLog::all();

        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'Logs';
        $log->user_id = Auth::user()->id;
        $log->action = 'Print';
        $log->save();

        $pdf = PDF::setPaper("letter");
        $pdf->loadView('logs.print',compact('data'));
        return $pdf->stream();
    }
}