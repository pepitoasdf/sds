<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schoolyear;
use App\SystemLog;
use Auth;
class SchoolYearController extends Controller
{
    public function  index(){
    	$sy = Schoolyear::orderBy('id','desc')->paginate(15);
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'School Year';
        $log->user_id = Auth::user()->id;
        $log->action = 'View';
        $log->save();
    	return view('sy.index',compact('sy'));
    }

    public function create(){
    	$effective = Schoolyear::get_effective();
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'School Year';
        $log->user_id = Auth::user()->id;
        $log->action = 'Create new';
        $log->save();
    	return view('sy.create',compact('effective'));
    }

    public function store(Request $request){
    	$this->validate($request, [
            'shortname' => 'required|min:1',
            'description' => 'required|min:5',
            'effective_date' => 'required|min:1'
        ]);
        if ( $user = Schoolyear::create($request->all()) ) {
            $request->session()->flash('alert-success', 'School Year has been created.');

        } else {
            $request->session()->flash('alert-danger', 'Unable to create chool Year.');
        }
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'School Year';
        $log->user_id = Auth::user()->id;
        $log->action = 'Store';
        $log->save();
        return redirect()->route('sy.index');
    }

    public function edit($id){
    	$data = Schoolyear::findOrFail($id);
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'School Year';
        $log->user_id = Auth::user()->id;
        $log->action = 'Edit';
        $log->save();
    	return view('sy.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'shortname' => 'required|min:1',
            'description' => 'required|min:5'
        ]);

      
        $data = Schoolyear::findOrFail($id);

        $data->fill($request->only('shortname', 'description'));
        if($data->getDirty()){
            $data->save();
            $request->session()->flash('alert-success', 'School Year has been updated.');
        }
        else{
            $request->session()->flash('alert-info', 'Nothing to Update.');
        }
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'School Year';
        $log->user_id = Auth::user()->id;
        $log->action = 'Update';
        $log->save();
        return redirect()->route('sy.index');
    }
}
