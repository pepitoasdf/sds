<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\StudentHeader;
use PDF;
use App\Schoolyear;
use MPDF;
use Auth;
use App\GuestPermission;
use App\SystemLog;
use App\Yearlevel;
class StudentDirectoryController extends Controller
{
	public function __construct(){
        $this->count = StudentHeader::count();
    }

    public function guest_create(){ abort(404);
        $id = 0;
        $syactive = Schoolyear::get_effective();
        return view('main-form',compact('id','syactive'));
    }

    public function guest_save(Request $request){ abort(404);
        $student =  new StudentHeader;
        $student->processRequest($request,$this->count,'No');
        $student->save();

        $request->session()->flash('alert-success', 'Your Information was Successfully sent to Admin\'s Account.');
        return redirect()->action('StudentDirectoryController@responsepage');
    }
    public function responsepage(){ abort(404);
        if(GuestPermission::get_guest()->form == 'No'){
            abort(404);
        }
        return view('responsepage');
    }
    public function index(Request $request, $id){
        if(!auth()->user()->can('view_students')){
            abort(404);
        }
    	$course = Course::findOrFail($id);
        $fid = $request->input('filter');
        $fid2 = $request->input('filter2');

        if($fid && $fid2){
            $student = StudentHeader::where('course_id',$id)
                        ->where('sy_id',$fid)
                        ->where('yearlevel_id',$fid2)
                        ->where('is_accepted','Yes')
                        ->orderBy('student_lastname','asc')
                        ->orderBy('status','asc')
                        ->paginate(15);
        }
        else if($fid){
            $student = StudentHeader::where('course_id',$id)
                        ->where('sy_id',$fid)
                        ->where('is_accepted','Yes')
                        ->orderBy('student_lastname','asc')
                        ->orderBy('status','asc')
                        ->paginate(15);
        }
        else if($fid2){
            $student = StudentHeader::where('course_id',$id)
                        ->where('yearlevel_id',$fid2)
                        ->where('is_accepted','Yes')
                        ->orderBy('student_lastname','asc')
                        ->orderBy('status','asc')
                        ->paginate(15);
        }
        else{
            $student = StudentHeader::where('course_id',$id)
                    ->where('is_accepted','Yes')
                    ->orderBy('student_lastname','asc')
                    ->orderBy('status','asc')
                    ->paginate(15);
        }


    	$log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'View';
        $log->save();

    	return view('students.index',compact('course','student','id','fid','fid2'));
    }
    public function printall(Request $request,$id){
        if(!auth()->user()->can('print_students')){
            abort(403);
        }

    	$course = Course::findOrFail($id);
        $fid = $request->input('filter');
        $fid2 = $request->input('filter2');
        if($fid && $fid2){
            $students = StudentHeader::where('course_id',$id)
                    ->where('sy_id',$fid)
                    ->where('yearlevel_id',$fid2)
                    ->orderBy('student_lastname','asc')
                    ->get();
        }
        elseif($fid){
            $students = StudentHeader::where('course_id',$id)
                    ->where('sy_id',$fid)
                    ->orderBy('student_lastname','asc')
                    ->get();
        }
        elseif($fid2){
            $students = StudentHeader::where('course_id',$id)
                    ->where('yearlevel_id',$fid2)
                    ->orderBy('student_lastname','asc')
                    ->get();
        }
        else{
            $students = StudentHeader::where('course_id',$id)->orderBy('student_lastname','asc')->get();
        }
        
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'Print All';
        $log->save();

        $pdf = MPDF::loadView('students.printall', compact('students','course'),[],['format' => 'letter']);
        return $pdf->stream($course->shortname.'.pdf');
    }
    public function print_id(Request $request,$id){
        if(!auth()->user()->can('print_students')){
            abort(403);
        }

        $course = Course::findOrFail($id);
        $fid = $request->input('filter');
        $fid2 = $request->input('filter2');
        $sy = null;
        $yl = null;
        if($fid && $fid2){
            $students = StudentHeader::where('course_id',$id)
                    ->where('sy_id',$fid)
                    ->where('yearlevel_id',$fid2)
                    ->orderBy('student_lastname','asc')
                    ->get();
            $sy = Schoolyear::find($fid);
            $yl = Yearlevel::find($fid2);
        }
        elseif($fid){
            $students = StudentHeader::where('course_id',$id)
                    ->where('sy_id',$fid)
                    ->orderBy('student_lastname','asc')
                    ->get();
            $sy = Schoolyear::find($fid);
        }
        elseif($fid2){
            $students = StudentHeader::where('course_id',$id)
                    ->where('yearlevel_id',$fid2)
                    ->orderBy('student_lastname','asc')
                    ->get();
            $yl = Yearlevel::find($fid2);
        }
        else{
            $students = StudentHeader::where('course_id',$id)->orderBy('student_lastname','asc')->get();
        }
        
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'Print IDs';
        $log->save();

        $pdf = MPDF::loadView('students.print_id', compact('students','course','sy','yl'),[],['format' => 'letter']);
        return $pdf->stream($course->shortname.'.pdf');
    }
     public function print_each($id,$ref_id){
        if(!auth()->user()->can('print_students')){
            abort(403);
        }
        $course = Course::find($id);
        $student = StudentHeader::where('id',$ref_id)->first();
        
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'Print Each';
        $log->save();

        $pdf = MPDF::loadView('students.print_each', compact('student','course'),[],['format' => 'letter']);
        return $pdf->stream($course->shortname.'_'.$student->student_firstname.'_'.$student->student_lastname.'.pdf');
    }
    public function create($id){
        if(!auth()->user()->can('add_students')){
            abort(403);
        }
        $syactive = Schoolyear::get_effective();
        $course = Course::find($id);

        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'Create New';
        $log->save();

        return view('students.create',compact('id','syactive'));
    }

    public function store(Request $request,$id){
        if(!auth()->user()->can('add_students')){
            abort(403);
        }
        $student =  new StudentHeader;
        $student->processRequest($request,$this->count,'Yes');
        $student->save();

        $course = Course::find($id);
        
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'Store';
        $log->save();

        $request->session()->flash('alert-success', 'Student Successfully Saved.');
        return redirect()->action('StudentDirectoryController@index',['id' => $id]);
    }

    public function edit($id,$ref){
        if(!auth()->user()->can('edit_students')){
            abort(403);
        }
        $course = Course::findOrFail($id);
        $student = StudentHeader::findOrFail($ref);

        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'Edit';
        $log->save();
        return view('students.edit',compact('id','course','student'));
    }

    public function update(Request $request,$id,$ref){
        if(!auth()->user()->can('edit_students')){
            abort(403);
        }
        
        $student =  StudentHeader::findOrFail($ref);
        $course = Course::findOrFail($id);
        if($student){
            $student->processRequest($request,$this->count,'Yes');
            $student->update();
            $request->session()->flash('alert-success', 'Student Successfully Updated.');
        }
        else{
            $request->session()->flash('alert-danger', 'Could not Update. Try Again');
        }
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = $course->shortname;
        $log->user_id = Auth::user()->id;
        $log->action = 'Update';
        $log->save();
        return redirect()->action('StudentDirectoryController@index',['id' => $id]);
    }
}
