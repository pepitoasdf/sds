<?php

namespace App\Http\Controllers;

use App\Authorizable;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use App\TabMenu;
use App\GuestPermission;
use App\SystemLog;
use Auth;
class RoleController extends Controller
{
    use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $menus = TabMenu::all();

        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Roles';
        $log->user_id = Auth::user()->id;
        $log->action = 'View';
        $log->save();
        
        return view('role.index', compact('roles', 'menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:roles']);

        if( Role::create($request->only('name')) ) {
            $request->session()->flash('alert-success', 'Role Added.');
            return redirect()->route('roles.index');
        }
        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Roles';
        $log->user_id = Auth::user()->id;
        $log->action = 'Store';
        $log->save();
        return redirect()->back();
    }

    public function edit($id){
        if($id == 0){
            $role = (object)['name' => 'Guest'];
        }
        else{
            $role = Role::findOrFail($id);
            if(!$role){
                abort(404);
            }
        }
        $menus = TabMenu::all();

        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Roles';
        $log->user_id = Auth::user()->id;
        $log->action = 'Edit Roles';
        $log->save();

        return view('role.edit', compact('role', 'menus'));
    }

    public function show($id){
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Roles';
        $log->user_id = \Auth::user()->id;
        $log->action = 'View';
        $log->save();
        return $this->edit($id);
    }

    public function create(){
        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Roles';
        $log->user_id = Auth::user()->id;
        $log->action = 'Create New';
        $log->save();
        return view('role.new');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == 0){
            $guest = GuestPermission::first();
            $guest->form = ($request->input('guestform')) ? 'Yes' : 'No';
            $guest->register = ($request->input('guestregister')) ? 'Yes' : 'No';
            $guest->save();
            $request->session()->flash('alert-success', 'Guest permissions has been updated.');
        }
        else{
            if($role = Role::findOrFail($id)) {
                // admin role has everything
                if($role->name === 'Admin') {
                    $role->syncPermissions(Permission::all());
                    return redirect()->route('roles.index');
                }

                $permissions = $request->get('permissions', []);

                $role->syncPermissions($permissions);

                $request->session()->flash('alert-success', $role->name . ' permissions has been updated.');
            } else {
                $request->session()->flash('alert-danger', 'Role with id '. $id .' note found.');
            }
        }
        $log = new SystemLog;
        $log->main_tab = 'User Management';
        $log->sub_tab = 'Roles';
        $log->user_id = Auth::user()->id;
        $log->action = 'Update';
        $log->save();
        return redirect()->route('roles.index');
    }
}
