<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\StudentHeader;
use PDF;
use App\Authorizable;
use App\User;
use App\SystemLog;
use Auth;
class CoursesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Course::orderBy('id','desc')->paginate(15);
        if($data){
            $log = new SystemLog;
            $log->main_tab = 'System Properties';
            $log->sub_tab = 'Courses';
            $log->user_id = Auth::user()->id;
            $log->action = 'View';
            $log->save();
        }
        return view('courses.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'shortname' => 'required|min:1',
            'description' => 'required|min:5'
        ]);

        // Create the user
        if ( $user = Course::create($request->all()) ) {
            $log = new SystemLog;
            $log->main_tab = 'System Properties';
            $log->sub_tab = 'Courses';
            $log->user_id = Auth::user()->id;
            $log->action = 'Store';
            $log->save();

            $request->session()->flash('alert-success', 'Course has been created.');

        } else {
            $request->session()->flash('alert-danger', 'Unable to create Course.');
        }

        return redirect()->route('courses.index');
    }

    public function edit($id){
        $data = Course::findOrFail($id);
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'Courses';
        $log->user_id = Auth::user()->id;
        $log->action = 'View Edit Form';
        $log->save();
        return view('courses.edit',compact('data'));
    }

    public function show($id){
       
    }

    public function create(){
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'Courses';
        $log->user_id = Auth::user()->id;
        $log->action = 'Create New';
        $log->save();
        return view('courses.new');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'shortname' => 'required|min:1',
            'description' => 'required|min:5'
        ]);

        // Get the user
        $course = Course::findOrFail($id);
        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'Courses';
        $log->user_id = Auth::user()->id;
        $log->action = 'Update';
        $log->save();
        // Update user
        $course->fill($request->only('shortname', 'description'));
        if($course->getDirty()){
            $course->save();
            $request->session()->flash('alert-success', 'Course has been updated.');
        }
        else{
            $request->session()->flash('alert-info', 'Nothing to Update.');
        }

        return redirect()->route('courses.index');
    }

    public function destroy(Request $request,$id)
    {
        $course = Course::findOrFail($id);
        if($course){
            $course->delete();
            $log = new SystemLog;
            $log->main_tab = 'System Properties';
            $log->sub_tab = 'Courses';
            $log->user_id = Auth::user()->id;
            $log->action = 'Delete';
            $log->save();
            $request->session()->flash('alert-success', 'Course successfully Removed.');
        }

        return redirect()->back();
    }

    public function printall(){
        if(!auth()->user()->can('print_courses')){
            abort(403);
        }
        
        $data = Course::all();

        $log = new SystemLog;
        $log->main_tab = 'System Properties';
        $log->sub_tab = 'Courses';
        $log->user_id = Auth::user()->id;
        $log->action = 'Print';
        $log->save();

        $pdf = PDF::setPaper("letter");
        $pdf->loadView('courses.print',compact('data'));
        return $pdf->stream();
    }
}
