<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\StudentHeader;
use App\SystemLog;
use Auth;
class StudentController extends Controller
{
	public function __construct(){
		$this->count = StudentHeader::count();
	}
	
    public function formIndex(){
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = 'Student Directory';
        $log->user_id = Auth::user()->id;
        $log->action = 'List';
        $log->save();
        return view('students.form');
    }

    public function formInsert(Request $request){
    	$student =  new StudentHeader;
    	$student->processRequest($request,$this->count);
    	$student->save();

        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = 'Student Directory';
        $log->user_id = Auth::user()->id;
        $log->action = 'Store';
        $log->save();

    	$request->session()->flash('success', 'Form Successfully Submitted');
    	return redirect()->action('StudentController@submmitted',['id' => $student->id]);
    }

    public function submmitted($id){
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = 'Student Directory';
        $log->user_id = \Auth::user()->id;
        $log->action = 'View';
        $log->save();

        return view('students.submitted_result');
    }

    public function formProfile($id){
    	$student =  StudentHeader::findOrFail($id);
        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = 'Student Directory';
        $log->user_id = Auth::user()->id;
        $log->action = 'Profile';
        $log->save();
    	return view('students.form',compact('student'));
    }

    public function formUpdate(Request $request,$id){
    	$student =  StudentHeader::findOrFail($id);
    	$student->processRequest($request,$this->count,'Yes');
    	$student->update();

        $log = new SystemLog;
        $log->main_tab = 'Student Directory';
        $log->sub_tab = 'Student Directory';
        $log->user_id = Auth::user()->id;
        $log->action = 'Update';
        $log->save();

    	$request->session()->flash('success', 'Form Successfully Updated');
    	return redirect()->action('StudentController@formProfile', ['id' => $student->id]);
    }
}
