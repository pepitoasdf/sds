<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Province;
use App\City;
use App\Barangay;
use Response;
class PlacesController extends Controller
{
    public function getprovinces(Request $request){
    	$data = Province::where('provDesc','like','%'.trim($request->input('name')).'%')->select("provCode as id","provDesc as text")->get();
    	return Response::json($data,200,array(),JSON_PRETTY_PRINT);
    }

    public function getcitymun(Request $request){
    	$ref_id = str_pad($request->input('ref_id'),4,"0",STR_PAD_LEFT);
    	$data = City::where('citymunDesc','like','%'.trim($request->input('name')).'%')
    			->where('provCode',$ref_id)
    			->select("citymunCode as id","citymunDesc as text")->get();

    	return Response::json($data,200,array(),JSON_PRETTY_PRINT);
    }

    public function getbarangays(Request $request){
    	$ref_id1 = str_pad($request->input('ref_id1'),4,"0",STR_PAD_LEFT);
    	$ref_id2 = str_pad($request->input('ref_id2'),6,"0",STR_PAD_LEFT);

    	$data = Barangay::where('brgyDesc','like','%'.trim($request->input('name')).'%')
    			->where('provCode',$ref_id1)
    			->where('citymunCode',$ref_id2)
    			->select("id","brgyDesc as text")
    			->get();

    	return Response::json($data,200,array(),JSON_PRETTY_PRINT);
    }
}
