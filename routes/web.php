<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (\Auth::check()){
    	return redirect()->route('dashboard-index');
	}
	else{
		return redirect()->route('login');
	}
});
Route::get('/form', 'StudentDirectoryController@guest_create')->name('guest_create');
Route::post('/form', 'StudentDirectoryController@guest_save')->name('guest_save');
Route::get('/response', 'StudentDirectoryController@responsepage')->name('guest_response');
Auth::routes();
Route::middleware('auth')->group(function () {
Route::middleware('appex')->group(function () {
	Route::resource('/systems/users', 'UserController');
    Route::resource('/systems/roles', 'RoleController');
    Route::resource('/systems/permissions', 'RoleController');
    Route::resource('/systems/settings', 'RoleController');
    Route::get('/systems/users/print/list','UserController@printall')->name('user-print');
    Route::get('/systems/profile', 'UserController@profile')->name('profile');
    Route::post('/systems/profile', 'UserController@updateProfile')->name('update-profile');
    Route::get('/systems/logs','UserController@logs_index')->name('logs.index');
    Route::get('/systems/logs/print','UserController@log_print')->name('logs.print');

    Route::get('/directory/{id}', 'StudentDirectoryController@index')->name('directory-index');
    Route::get('/directory/printall/{id}', 'StudentDirectoryController@printall')->name('directory-printall');
    Route::get('/directory/printid/{id}', 'StudentDirectoryController@print_id')->name('directory-print_id');
    Route::get('/directory/{id}/print/{ref_id}', 'StudentDirectoryController@print_each')->name('directory-print_each');
    Route::get('/directory/{id}/create', 'StudentDirectoryController@create')->name('directory-create');
    Route::post('/directory/{id}/create', 'StudentDirectoryController@store')->name('directory-store');
    Route::get('/directory/{id}/edit/{ref_id}', 'StudentDirectoryController@edit')->name('directory-edit');
    Route::post('/directory/{id}/edit/{ref_id}', 'StudentDirectoryController@update')->name('directory-update');

    Route::resource('/settings/courses','CoursesController');
    Route::get('/settings/courses/print/list','CoursesController@printall')->name('courses-print');


	Route::get('/search', 'HomeController@getsearch_global')->name('search-display');
	Route::get('/courses/notification/show', 'HomeController@notification')->name('notify-display');

    Route::get('/home', 'HomeController@index')->name('dashboard-index');
    Route::get('/untrack/recent', 'HomeController@recent')->name('recent-list');
    Route::post('/untrack/recent', 'HomeController@profileMany')->name('recent-many');
    Route::delete('/untrack/recent/{id}', 'HomeController@remove')->name('recent-remove');
    Route::put('/untrack/recent/{id}', 'HomeController@update')->name('recent-update');
    Route::get('/untrack/view_profile/{id}', 'HomeController@view_profile')->name('recent-view');
    Route::post('/untrack/view_profile/{id}', 'HomeController@profile')->name('recent-profile');
    Route::get('/untrack/mark/', 'HomeController@mark')->name('recent-mark');
    Route::get('/untrack/notify/btn', 'HomeController@btnnotify')->name('recent-notify');
    Route::get('/settings/sy/index', 'SchoolYearController@index')->name('sy.index');
    Route::get('/settings/sy/create', 'SchoolYearController@create')->name('sy.create');
    Route::post('/settings/sy/create', 'SchoolYearController@store')->name('sy.store');
    Route::get('/settings/sy/{id}', 'SchoolYearController@edit')->name('sy.edit');
    Route::patch('/settings/sy/{id}', 'SchoolYearController@update')->name('sy.update');

    Route::get('/search', 'HomeController@getsearch_global')->name('search');
});
});
Route::get('/places/provinces', 'PlacesController@getprovinces')->name('provinces');
Route::get('/places/citymun', 'PlacesController@getcitymun')->name('citymun');
Route::get('/places/barangays', 'PlacesController@getbarangays')->name('barangays');