/******* Pagination *******/
 $('ul.pagination').addClass('pagination-basic mg-b-0');
 $('ul.pagination li').addClass('page-item');
 $('ul.pagination li a,ul.pagination li span').addClass('page-link');
 /***** end Pagination *****/

/*** Sticky footer ***/

function extraMargin() {
	$(".kt-pagebody").css({'min-height' : ''});
	var doc = $(document).outerHeight(true);
	var t = $(".kt-pagetitle").outerHeight(true);
	var b = $(".kt-pagebody").outerHeight(true);
	var f = $(".kt-footer").outerHeight(true);
    var h = (doc - (t + b + f)) + 35;
	$(".kt-pagebody").css({'min-height' : h+'px'});
}
$(document).ready(function(){
    extraMargin();
});
$(window).resize(function(){
    extraMargin();
});
/** end sticky footer **/
/***** notification********/
/*******commented*********
$('.notifybtn').click(function(e){
    e.preventDefault();
    $('body').find('.notification-display').html('\
    	<div class="sk-circle">\
                <div class="sk-circle1 sk-child"></div>\
                <div class="sk-circle2 sk-child"></div>\
                <div class="sk-circle3 sk-child"></div>\
                <div class="sk-circle4 sk-child"></div>\
                <div class="sk-circle5 sk-child"></div>\
                <div class="sk-circle6 sk-child"></div>\
                <div class="sk-circle7 sk-child"></div>\
                <div class="sk-circle8 sk-child"></div>\
                <div class="sk-circle9 sk-child"></div>\
                <div class="sk-circle10 sk-child"></div>\
                <div class="sk-circle11 sk-child"></div>\
                <div class="sk-circle12 sk-child"></div>\
        </div>\
    ');
    $.get('/courses/notification/show',function(data){
    	$('body').find('.notification-display').html(data);
    });
});
setInterval(function(){
    $.get('/untrack/notify/btn',function(data){
        if(data != 0){
            $('body').find('.nx').addClass('bg-danger');
        }
        else{
            $('body').find('.nx').removeClass('text-danger').removeClass('bg-danger');
        }

    });
},10000);
*/
/***** end notification*******