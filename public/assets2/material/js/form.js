$('#form1of1 select').select2();
$('#form1of2 select').select2();
$(".province").select2({
	placeholder: 'Provinces',
	minimumInputLength : 1,
	allowClear: false,
	ajax : {
		url : "/places/provinces",
		dataType : 'json',
		data : function (params) {
			return {
				name: params.term
			};
		},
		processResults: function (data) {
			return {
				results: data
			};
		},
		cache: true
	},
	escapeMarkup: function (markup) { return markup; },
	templateResult: function(repo) {
		return repo.text;
	},
	templateSelection: function(repo){
		return repo.text;
	}
});

$('.city_mun').select2({
    placeholder: 'City/Municipality',
    minimumInputLength : 1,
	allowClear: false,
	ajax : {
		url : "/places/citymun",
		dataType : 'json',
		data : function (params) {
			var prov = $(this).closest('div').find('.province').val();
			return {
				name: params.term,
				ref_id: prov
			};
		},
		processResults: function (data) {
			return {
				results: data
			};
		},
		cache: true
	},
	escapeMarkup: function (markup) { return markup; },
	templateResult: function(repo) {
		return repo.text;
	},
	templateSelection: function(repo){
		return repo.text;
	}
});

$('.barangay').select2({
  	placeholder: 'Barangay',
  	minimumInputLength : 1,
	allowClear: false,
	ajax : {
		url : "/places/barangays",
		dataType : 'json',
		data : function (params) {
			var prov = $(this).closest('div').find('.province').val();
			var cm = $(this).closest('div').find('.city_mun').val();
			return {
				name: params.term,
				ref_id1: prov,
				ref_id2: cm
			};
		},
		processResults: function (data) {
			return {
				results: data
			};
		},
		cache: true
	},
	escapeMarkup: function (markup) { return markup; },
	templateResult: function(repo) {
		return repo.text;
	},
	templateSelection: function(repo){
		return repo.text;
	}
});

$('.province').on('select2:select', function (evt) {
	$(this).closest('div').find('.city_mun').val(null).trigger('change.select2');
	$(this).closest('div').find('.barangay').val(null).trigger('change.select2');
});
$('.city_mun').on('select2:select', function (evt) {
	$(this).closest('div').find('.barangay').select2("val", null);
});

$(document).keyup(function(e){
	if(e.keyCode == 9){
		var el = $(this).find('.select2-container--focus').trigger('change');
	}
});

$('select').on('select2:close', function (evt) {
  	$(this).focus();
});

$('form#sdsform').find('textarea,input').on('click', function(e){
	$(this).css('border-color','#6DC9F7');
});

$('form#sdsform').find('.select2').on('click', function(e){
	$(this).find('.select2-selection--single').css('border-color','#6DC9F7');
});

$('form#sdsform').find('textarea,input').on('blur', function(e){
	$(this).css('border-color','rgba(0,0,0,.15)');
});

 $('.dropify').dropify();


 /******* Pagination *******/
 $('ul.pagination').addClass('pagination-sm');
 $('ul.pagination li').addClass('page-item');
 $('ul.pagination li a,ul.pagination li span').addClass('page-link');
 /***** end Pagination *****/

/***** validation *****/
// $("input,select,textarea").not("[type=submit]").jqBootstrapValidation({
//     preventSubmit: true,
//     submitSuccess: function (form, event) {
//         event.preventDefault();
//         dosubmit(form);
//     },
//     submitError: function (form, event, errors) {
//         event.preventDefault();
//         form.find('.select2-selection--single').css('border-color','red');

//         $.toast({
//             heading: 'Message',
//             text: 'Please fill all required fields.',
//             position: 'top-right',
//             loaderBg:'#c9742b',
//             icon: 'warning',
//             hideAfter: 10000, 
//             stack: 6
//         });
//     }
// });

$('.btnsubmit').click(function(){
	$(this).closest('form#sdsform').find('input[required][value=""]').css('border-color','#e74a4a');
	$(this).closest('form#sdsform').find('textarea[required]').each(function(){
		if($(this).val().trim() == ''){
			$(this).css('border-color','#e74a4a');
		}
	});
	$(this).closest('form#sdsform').find('.select2').find('.select2-selection--single').each(function(){
		var title = $(this).find('.select2-selection__rendered').attr('title');
		if(title == '' || title == null){
			$(this).css('border-color','#e74a4a');
		}
	});
});

$('form#sdsform').submit(function(){

});
/** End Validation **/
/********** Notification ***********/
$('.notifybtn').click(function(e){
    e.preventDefault();
    $('body').find('.notification-display').html('<div class="preloader">\
    	<svg class="circular" viewBox="25 25 50 50">\
       		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>\
		</div>');
    $.get('/courses/notification/show',function(data){
    	$('body').find('.notification-display').html(data);
    });
});
/************ Recent *************/
$('.recent_filter').on('change',function(e){
	e.preventDefault();
	var filter = $(this).val();
	window.location.href = "/home/recent?view="+filter;
});